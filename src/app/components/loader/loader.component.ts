import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
	selector: 'jw-loader',
	templateUrl: './loader.component.html',
	styleUrls: ['./loader.component.less']
})
export class LoaderComponent implements OnInit {
	@Output() isLoadingChange: EventEmitter<boolean> = new EventEmitter();
	@Input() set isLoading(value: boolean) {
		this.setLoaderVisible(value);
	}

	constructor() { }

	ngOnInit() {
	}

	setLoaderVisible(value: boolean) {
		if (value === undefined) {
			return;
		}

		if (value) {
			this.isLoadingChange.emit(value);
			this.addClass('jw-loader-animation-in');
			setTimeout(() => this.removeClass('jw-loader-animation-in'), 1000);
		} else {
			this.addClass('jw-loader-animation-out');
			setTimeout(() => {
				this.removeClass('jw-loader-animation-out');
				this.isLoadingChange.emit(value);
			}, 1000);
		}
	}

	addClass(className: string) {
		const node = document.querySelector('.jw-loader');
		node.classList.add(className);
	}

	removeClass(className: string) {
		const node = document.querySelector('.jw-loader');
		node.classList.remove(className);
	}
}
