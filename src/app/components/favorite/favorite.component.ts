import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from 'src/app/shared/services/items/items.service';

@Component({
	selector: 'jw-favorite',
	templateUrl: './favorite.component.html',
	styleUrls: ['./favorite.component.less']
})
export class FavoriteComponent implements OnInit {
	@Input() item: any;

	constructor(public itemsService: ItemsService) { }

	ngOnInit() {
	}
}
