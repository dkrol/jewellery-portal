import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { Paginator } from 'src/app/shared/models/Paginator';
import { PageInfo } from 'src/app/shared/models/PageInfo';
import { SortInfo } from 'src/app/shared/models/SortInfo';
import { UserPasswordComponent } from '../../user/user-password/user-password.component';
import { CustomNotificationsService } from 'src/app/shared/services/notifications/custom-notifications.service';
import { UserEditComponent } from '../../user/user-edit/user-edit.component';

@Component({
	selector: 'jw-users-table',
	templateUrl: './users-table.component.html',
	styleUrls: ['./users-table.component.less']
})
export class UsersTableComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	@Input() set usersData(response: any) { this.setUsersData(response); }
	@Output() usersDataUpdate: EventEmitter<any> = new EventEmitter<any>();

	public readonly displayedColumns: Array<string> = ['login', 'email', 'role', 'creationDate', 'modificationDate', 'isActive', 'actions'];
	public paginatorConfig: Paginator = Paginator.create().withPageSize(10);
	public usersTableDataSource: MatTableDataSource<any>;

	private _searchText: string;
	private _users: Array<any>;

	constructor(
		private dialog: MatDialog,
		private customNotificationsService: CustomNotificationsService
	) { }

	ngOnInit() {
		this.paginatorConfig.updatePaginator(this.paginator);
		this.updateUsersEvent();
	}

	public onSearchTextChanged(searchText: string) {
		this._searchText = searchText;
		this.updateUsersEvent();
	}

	public onSortChange() {
		this.updateUsersEvent();
	}

	public onPaginatorChange() {
		this.updateUsersEvent();
	}

	public onChangePassword(event, user) {
		event.stopPropagation();
		this.dialog.open(UserPasswordComponent, this.prepareUserPassword(user))
			.afterClosed()
			.subscribe(result => this.afterUserPasswordClose(result));
	}

	public onEdit(event, user) {
		event.stopPropagation();
		this.dialog.open(UserEditComponent, this.prepareUserEdit(user))
			.afterClosed()
			.subscribe(result => this.afterUserEditClose(result));
	}

	public onDelete(event, user) {
		event.stopPropagation();
	}

	private afterUserPasswordClose(result) {
		if (result) {
			this.updateUsersEvent();
			this.customNotificationsService.successTranslate('JW.STATEMENT', 'USER.CHANGE_PASSWORD_SUCCESS');
		}
	}

	private afterUserEditClose(result) {
		if (result) {
			this.updateUsersEvent();
			this.customNotificationsService.successTranslate('JW.STATEMENT', 'USER.EDIT_USER_SUCCESS');
		}
	}

	private setUsersData(response: any) {
		if (response) {
			this._users = response.results ? response.results : [];
			this.updateTableData();
			this.updatePaginator(response.totalCount);
		}
	}

	private updateTableData() {
		this.usersTableDataSource = new MatTableDataSource<any>(this._users);
	}

	private updatePaginator(totalCount: number) {
		this.paginatorConfig.length = totalCount;
	}

	private updateUsersEvent() {
		const request = this.prepareRequest();
		this.usersDataUpdate.emit(request);
	}

	private prepareRequest(): any {
		return {
			pageInfo: PageInfo.Create(this.paginator.pageIndex + 1, this.paginator.pageSize),
			sortInfo: SortInfo.Create(this.sort.active, this.sort.direction),
			searchText: this._searchText
		};
	}

	private prepareUserPassword(user) {
		const dialogConfig = this.prepareDialogConfigBase();
		dialogConfig.data = { user };
		return dialogConfig;
	}

	private prepareUserEdit(user) {
		const dialogConfig = this.prepareDialogConfigBase();
		dialogConfig.data = { user };
		return dialogConfig;
	}

	private prepareDialogConfigBase(): MatDialogConfig {
		const dialogConfigBase = new MatDialogConfig();
		dialogConfigBase.autoFocus = true;
		dialogConfigBase.disableClose = true;
		return dialogConfigBase;
	}
}
