import { Component, OnInit } from '@angular/core';
import { RegulationsApiService } from 'src/app/shared/services/api/regulations.api.service';

@Component({
	selector: 'jw-regulations',
	templateUrl: './regulations.component.html',
	styleUrls: ['./regulations.component.less']
})
export class RegulationsComponent implements OnInit {
	public regulations: string;

	constructor(
		private regulationsApiService: RegulationsApiService
	) { }

	ngOnInit() {
		this.initRegulations();
	}

	private initRegulations() {
		this.regulationsApiService.getRegulations()
			.subscribe(response => this.regulations = response);
	}
}
