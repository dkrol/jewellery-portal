import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageItemsSoldComponent } from './manage-items-sold.component';

describe('ManageItemsSoldComponent', () => {
  let component: ManageItemsSoldComponent;
  let fixture: ComponentFixture<ManageItemsSoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageItemsSoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageItemsSoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
