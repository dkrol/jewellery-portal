import { Component, OnInit } from '@angular/core';
import { GetItemsFilterPageQuery } from 'src/app/shared/domains/items/queries/GetItemsFilterPageQuery';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';

@Component({
	selector: 'jw-manage-items-inactive',
	templateUrl: './manage-items-inactive.component.html',
	styleUrls: ['./manage-items-inactive.component.less']
})
export class ManageItemsInactiveComponent implements OnInit {
	public readonly displayedColumns: Array<string> =
		['avatar', 'title', 'quantity', 'price', 'creationDate', 'modificationDate', 'isActive', 'actions'];
	public itemsData: any;

	constructor(
		private portalService: PortalService,
		private itemsApiService: ItemsApiService
	) { }

	ngOnInit() {
	}

	public itemsDataUpdate(request: any) {
		this.portalService.showLoader();
		request = this.prepareRequest(request);
		this.itemsApiService.getItemsFilterPage(request)
			.subscribe(response => this.getItemsFilterPageSuccess(response));
	}

	private getItemsFilterPageSuccess(response: any) {
		this.itemsData = response.result;
		this.portalService.hideLoader();
	}

	private prepareRequest(request: any) {
		request = request as GetItemsFilterPageQuery;
		request.isActive = false;
		return request;
	}
}
