import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageItemsInactiveComponent } from './manage-items-inactive.component';

describe('ManageItemsInactiveComponent', () => {
  let component: ManageItemsInactiveComponent;
  let fixture: ComponentFixture<ManageItemsInactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageItemsInactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageItemsInactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
