import { Component, OnInit } from '@angular/core';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';
import { GetItemsFilterPageQuery } from 'src/app/shared/domains/items/queries/GetItemsFilterPageQuery';

@Component({
	selector: 'jw-manage-items-active',
	templateUrl: './manage-items-active.component.html',
	styleUrls: ['./manage-items-active.component.less']
})
export class ManageItemsActiveComponent implements OnInit {
	public readonly displayedColumns: Array<string> =
		['avatar', 'title', 'quantity', 'price', 'creationDate', 'modificationDate', 'isActive', 'actions'];
	public itemsData: any;

	constructor(
		private portalService: PortalService,
		private itemsApiService: ItemsApiService
	) { }

	ngOnInit() {
	}

	public itemsDataUpdate(request: any) {
		this.portalService.showLoader();
		request = this.prepareRequest(request);
		this.itemsApiService.getItemsFilterPage(request)
			.subscribe(response => this.getItemsFilterPageSuccess(response));
	}

	private getItemsFilterPageSuccess(response: any) {
		this.itemsData = response.result;
		this.portalService.hideLoader();
	}

	private prepareRequest(request: any) {
		request = request as GetItemsFilterPageQuery;
		request.isActive = true;
		return request;
	}
}
