import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageItemsActiveComponent } from './manage-items-active.component';

describe('ManageItemsActiveComponent', () => {
	let component: ManageItemsActiveComponent;
	let fixture: ComponentFixture<ManageItemsActiveComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ManageItemsActiveComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ManageItemsActiveComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
