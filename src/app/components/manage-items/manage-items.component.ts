import { Component, OnInit } from '@angular/core';
import { ManageItemsTabEnum } from './enums/ManageItemsTabEnum';

@Component({
	selector: 'jw-manage-items',
	templateUrl: './manage-items.component.html',
	styleUrls: ['./manage-items.component.less']
})
export class ManageItemsComponent implements OnInit {
	public selectedTabIndex: number;
	public manageItemsTabEnum = ManageItemsTabEnum;

	constructor() { }

	ngOnInit() {
		this.selectedTabIndex = ManageItemsTabEnum.ActiveItems;
	}

	public selectedTabChange(event: any) {

	}
}
