import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InfoDialogMode } from 'src/app/shared/enums/InfoDialogMode';

@Component({
	selector: 'jw-info-dialog',
	templateUrl: './info-dialog.component.html',
	styleUrls: ['./info-dialog.component.less']
})
export class InfoDialogComponent implements OnInit {
	public readonly infoDialogModeEnum = InfoDialogMode;

	constructor(
		private dialogRef: MatDialogRef<InfoDialogComponent>,
		@Inject(MAT_DIALOG_DATA) private data) {
	}

	ngOnInit() {
	}
}
