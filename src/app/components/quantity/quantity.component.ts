import { Component, OnInit, Output, Input, EventEmitter, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { QuantityForm } from 'src/app/shared/forms/QuantityForm';

@Component({
	selector: 'jw-quantity',
	templateUrl: './quantity.component.html',
	styleUrls: ['./quantity.component.less'],
	encapsulation: ViewEncapsulation.None
})
export class QuantityComponent implements OnInit {
	@Input() max: number;
	@Input() quantity: number;
	@Output() quantityChanged: EventEmitter<number> = new EventEmitter();

	public min = 1;

	public quantityForm = new QuantityForm(this.min, this.max);

	constructor(
		private ref: ChangeDetectorRef
	) { }

	ngOnInit() {
		this.initQuantity();
	}

	public onInputChange(event) {
		console.log(event.data);
		// if (event.data) {
		// 	this.quantity = event.data;
		// 	this.updateQuantity();
		// } else {
		// 	this.initQuantity();
		// }
	}

	public onIncrementClick() {
		this.quantity++;
		this.updateQuantity();
	}

	public onDecrementClick() {
		this.quantity--;
		this.updateQuantity();
	}

	private initQuantity() {
		this.quantity = this.quantity ? this.quantity : this.min;
		this.updateQuantity();
	}

	private updateQuantity() {
		this.quantityForm.loadData(this.quantity);
		this.quantityChanged.emit(this.quantity);
	}
}
