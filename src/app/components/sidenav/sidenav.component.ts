import { Component, OnInit, ViewChild } from '@angular/core';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { MatSidenav } from '@angular/material';
import { MenuNodesService } from 'src/app/shared/services/menu/menu-nodes.service';

@Component({
	selector: 'jw-sidenav',
	templateUrl: './sidenav.component.html',
	styleUrls: ['./sidenav.component.less']
})
export class SidenavComponent implements OnInit {
	@ViewChild('sidenav') sidenav: MatSidenav;

	constructor(public menuNodesService: MenuNodesService, private portalService: PortalService) { }

	ngOnInit() {
		this.portalService.sidenav = this.sidenav;
	}
}
