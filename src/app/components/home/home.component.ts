import { Component, OnInit } from '@angular/core';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';
import { GetItemsPageQuery } from 'src/app/shared/domains/items/queries/GetItemsPageQuery';
import { PageInfo } from 'src/app/shared/models/PageInfo';
import { SortInfo } from 'src/app/shared/models/SortInfo';

@Component({
	selector: 'jw-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
	public readonly itemsNewCount: number = 6;

	public itemsNew: Array<any>;
	public itemsLastModified: Array<any>;

	constructor(private itemsApiService: ItemsApiService) { }

	ngOnInit() {
		this.initItemsNew();
	}

	private initItemsNew() {
		const sortInfo = SortInfo.Create('creationDate', 'desc');
		const request = this.prepareRequest(sortInfo);
		this.itemsApiService.getItemsPage(request)
			.subscribe((response: any) => this.getItemsNewPageSuccess(response));
	}

	private getItemsNewPageSuccess(response: any) {
		this.itemsNew = response.result && response.result.results ? response.result.results : [];
	}

	private prepareRequest(sortInfo: SortInfo): GetItemsPageQuery {
		const request = new GetItemsPageQuery();
		request.pageInfo = PageInfo.Create(1, this.itemsNewCount);
		request.sortInfo = sortInfo;
		return request;
	}
}
