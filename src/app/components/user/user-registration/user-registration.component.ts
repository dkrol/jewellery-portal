import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UserRegisterForm as UserRegistrationForm } from 'src/app/shared/forms/UserRegistrationForm';
import { UserFormMode } from 'src/app/shared/enums/UserFormMode';

@Component({
	selector: 'jw-user-form',
	templateUrl: './user-registration.component.html',
	styleUrls: ['./user-registration.component.less']
})
export class UserRegistrationComponent implements OnInit {
	@Input() mode: UserFormMode;
	@Input('userForm') user: UserRegistrationForm;
	@Output('onSave') onSaveChanged: EventEmitter<UserRegistrationForm> = new EventEmitter();
	@Output('onCancel') onCancelChanged: EventEmitter<any> = new EventEmitter();

	public userFormMode = UserFormMode;
	public regulations = false;

	constructor() { }

	ngOnInit() {
	}

	public onSaveClick() {
		this.onSaveChanged.emit(this.user);
	}

	public onCancelClick() {
		this.onCancelChanged.emit();
	}
}
