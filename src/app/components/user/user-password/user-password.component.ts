import { Component, OnInit, Inject } from '@angular/core';
import { UserPasswordForm } from 'src/app/shared/forms/UserPasswordForm';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsersApiService } from 'src/app/shared/services/api/users.api.service';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { ChangeUserPasswordCommand } from 'src/app/shared/domains/users/commands/ChangeUserPasswordCommand';
import { AuthService } from 'src/app/shared/services/auth/auth-service.service';

@Component({
	selector: 'jw-user-password',
	templateUrl: './user-password.component.html',
	styleUrls: ['./user-password.component.less']
})
export class UserPasswordComponent implements OnInit {
	public password: UserPasswordForm = new UserPasswordForm();

	constructor(
		private authService: AuthService,
		private portalService: PortalService,
		private usersApiService: UsersApiService,
		private dialogRef: MatDialogRef<UserPasswordComponent>,
		@Inject(MAT_DIALOG_DATA) private data) { }

	ngOnInit() {
		this.password.loadData(this.data.user);
	}

	public onSaveClick() {
		if (!this.password.form.invalid) {
			this.portalService.showLoader();
			const request = this.prepareRequest();

			if (this.authService.isAdminRole) {
				this.usersApiService.changeUserPassword(request)
					.subscribe(response => this.changeUserPasswordSuccess(response));
			} else {
				this.usersApiService.changeCurrentUserPassword(request.password)
					.subscribe(response => this.changeUserPasswordSuccess(response));
			}
		}
	}

	private prepareRequest(): ChangeUserPasswordCommand {
		const request = this.password.parseJson();
		return request as ChangeUserPasswordCommand;
	}

	private changeUserPasswordSuccess(response: any) {
		this.dialogRef.close(true);
		this.portalService.hideLoader();
	}
}
