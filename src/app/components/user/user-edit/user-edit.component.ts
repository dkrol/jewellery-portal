import { Component, OnInit, Inject } from '@angular/core';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { UsersApiService } from 'src/app/shared/services/api/users.api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserEditForm } from 'src/app/shared/forms/UserEditForm';
import { RolesApiService } from 'src/app/shared/services/api/roles.api.service';
import { AuthService } from 'src/app/shared/services/auth/auth-service.service';

@Component({
	selector: 'jw-user-edit',
	templateUrl: './user-edit.component.html',
	styleUrls: ['./user-edit.component.less']
})
export class UserEditComponent implements OnInit {
	public userEdit = new UserEditForm();
	public roles: Array<any> = [];

	constructor(
		public authService: AuthService,
		private portalService: PortalService,
		private usersApiService: UsersApiService,
		private rolesApiService: RolesApiService,
		private dialogRef: MatDialogRef<UserEditComponent>,
		@Inject(MAT_DIALOG_DATA) private data
	) { }

	ngOnInit() {
		this.initRoles();
		this.userEdit.loadData(this.data.user);
	}

	public onSaveClick() {
		if (!this.userEdit.form.invalid) {
			this.portalService.showLoader();
			const request = this.userEdit.parseJson();
			this.usersApiService.updateUser(request)
				.subscribe(response => this.updateUserSuccess(response));
		}
	}

	private initRoles() {
		this.rolesApiService.getRoles()
			.subscribe(response => this.roles = response && response.result ? response.result : []);
	}

	private updateUserSuccess(response) {
		this.dialogRef.close(true);
		this.portalService.hideLoader();
	}
}
