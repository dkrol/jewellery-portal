import { Component, OnInit, Inject } from '@angular/core';
import { UserEmailForm } from 'src/app/shared/forms/UserEmailForm';
import { ChangeUserEmailCommand } from 'src/app/shared/domains/users/commands/ChangeUserEmailCommand';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { UsersApiService } from 'src/app/shared/services/api/users.api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from 'src/app/shared/services/auth/auth-service.service';

@Component({
	selector: 'jw-user-email',
	templateUrl: './user-email.component.html',
	styleUrls: ['./user-email.component.less']
})
export class UserEmailComponent implements OnInit {
	public email: UserEmailForm = new UserEmailForm();

	constructor(
		private authService: AuthService,
		private portalService: PortalService,
		private usersApiService: UsersApiService,
		private dialogRef: MatDialogRef<UserEmailComponent>,
		@Inject(MAT_DIALOG_DATA) private data) { }

	ngOnInit() {
		this.email.loadData(this.data.user);
	}

	public onSaveClick() {
		if (!this.email.form.invalid) {
			this.portalService.showLoader();
			const request = this.prepareRequest();
			this.usersApiService.changeUserEmail(request)
				.subscribe(response => this.changeEmailPasswordSuccess(response));
		}
	}

	private prepareRequest(): ChangeUserEmailCommand {
		const request = this.email.parseJson();
		return request as ChangeUserEmailCommand;
	}

	private changeEmailPasswordSuccess(response: any) {
		this.dialogRef.close(true);
		this.portalService.hideLoader();
	}
}
