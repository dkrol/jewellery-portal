import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageUsersAllComponent } from './manage-users-all.component';

describe('ManageUsersAllComponent', () => {
  let component: ManageUsersAllComponent;
  let fixture: ComponentFixture<ManageUsersAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageUsersAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageUsersAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
