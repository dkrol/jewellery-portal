import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageUsersActiveComponent } from './manage-users-active.component';

describe('ManageUsersActiveComponent', () => {
  let component: ManageUsersActiveComponent;
  let fixture: ComponentFixture<ManageUsersActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageUsersActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageUsersActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
