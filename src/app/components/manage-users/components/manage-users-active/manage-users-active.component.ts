import { Component, OnInit } from '@angular/core';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { UsersApiService } from 'src/app/shared/services/api/users.api.service';
import { GetUsersFilterPageQuery } from 'src/app/shared/domains/users/queries/GetUsersFilterPageQuery';

@Component({
	selector: 'jw-manage-users-active',
	templateUrl: './manage-users-active.component.html',
	styleUrls: ['./manage-users-active.component.less']
})
export class ManageUsersActiveComponent implements OnInit {
	public usersData;

	constructor(
		private portalService: PortalService,
		private usersApiService: UsersApiService
	) { }

	ngOnInit() {
	}

	public usersDataUpdate(request: any) {
		this.portalService.showLoader();
		request = this.prepareRequest(request);
		this.usersApiService.getUsersFilterPage(request)
			.subscribe(response => this.getItemsFilterPageSuccess(response));
	}

	private getItemsFilterPageSuccess(response: any) {
		this.usersData = response.result;
		this.portalService.hideLoader();
	}

	private prepareRequest(request: any): GetUsersFilterPageQuery {
		request = request as GetUsersFilterPageQuery;
		request.isActive = true;
		return request;
	}
}
