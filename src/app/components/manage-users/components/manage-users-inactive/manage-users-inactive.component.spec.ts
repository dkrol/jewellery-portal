import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageUsersInactiveComponent } from './manage-users-inactive.component';

describe('ManageUsersInactiveComponent', () => {
  let component: ManageUsersInactiveComponent;
  let fixture: ComponentFixture<ManageUsersInactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageUsersInactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageUsersInactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
