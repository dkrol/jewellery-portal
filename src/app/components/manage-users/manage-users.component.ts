import { Component, OnInit } from '@angular/core';
import { ManageUsersTabEnum } from './enums/ManageUsersTabEnum';

@Component({
	selector: 'jw-manage-users',
	templateUrl: './manage-users.component.html',
	styleUrls: ['./manage-users.component.less']
})
export class ManageUsersComponent implements OnInit {
	public selectedTabIndex: number;
	public manageUsersTabEnum = ManageUsersTabEnum;

	constructor() { }

	ngOnInit() {
		this.selectedTabIndex = ManageUsersTabEnum.AllUsers;
	}

	public selectedTabChange(event: any) {

	}
}
