import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { SortInfo } from 'src/app/shared/models/SortInfo';
import { SortOption } from 'src/app/shared/models/SortOption';

@Component({
	selector: 'jw-sort',
	templateUrl: './sort.component.html',
	styleUrls: ['./sort.component.less'],
	encapsulation: ViewEncapsulation.None
})
export class SortComponent implements OnInit {
	@Input() set sortInfo(sortInfo: SortInfo) { this.setSelectedBySortInfo(sortInfo); }
	@Output() sortChanged: EventEmitter<SortInfo> = new EventEmitter();

	private readonly CREATION_DATE = 'creationDate';
	private readonly PRICE = 'price';
	private readonly POPULARITY = 'popularity';
	private readonly ASC = 'asc';
	private readonly DESC = 'desc';

	public sortOptions: Array<SortOption>;
	public selected: SortOption;

	constructor() {
		this.initSortOptions();
	}

	ngOnInit() {}

	public onSortChanged() {
		this.sortChanged.emit(this.selected.sortInfo);
	}

	private setSelectedBySortInfo(sortInfo: SortInfo) {
		const result = this.sortOptions
			.find(sort => sort.sortInfo.property === sortInfo.property &&
				sort.sortInfo.direction === sortInfo.direction);

		if (result) {
			this.selected = result;
		}
	}

	private initSortOptions() {
		this.sortOptions = [
			SortOption.Create('ADDITION_TIME_LATEST', SortInfo.Create(this.CREATION_DATE, this.DESC)),
			SortOption.Create('ADDITION_TIME_OLDEST', SortInfo.Create(this.CREATION_DATE, this.ASC)),
			SortOption.Create('PRICE_LOWEST', SortInfo.Create(this.PRICE, this.ASC)),
			SortOption.Create('PRICE_HIGHEST', SortInfo.Create(this.PRICE, this.DESC)),
			SortOption.Create('POPULARITY_LARGEST', SortInfo.Create(this.POPULARITY, this.DESC))
		];
	}
}
