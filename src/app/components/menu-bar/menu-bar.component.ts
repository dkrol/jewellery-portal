import { Component, OnInit } from '@angular/core';
import { CategoriesApiService } from 'src/app/shared/services/api/categories.api.service';
import { Router } from '@angular/router';

@Component({
	selector: 'jw-menu-bar',
	templateUrl: './menu-bar.component.html',
	styleUrls: ['./menu-bar.component.less']
})
export class MenuBarComponent implements OnInit {

	public categories: any = {};
	constructor(private categoriesApiService: CategoriesApiService) { }

	ngOnInit() {
		this.initCategories();
	}

	private initCategories() {
		this.categories = this.categoriesApiService.categories;
	}
}
