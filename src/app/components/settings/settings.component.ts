import { Component, OnInit } from '@angular/core';
import { UsersApiService } from 'src/app/shared/services/api/users.api.service';
import { AuthService } from 'src/app/shared/services/auth/auth-service.service';
import { UserPasswordComponent } from '../user/user-password/user-password.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { CustomNotificationsService } from 'src/app/shared/services/notifications/custom-notifications.service';
import { UserEmailComponent } from '../user/user-email/user-email.component';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { SettingsApiService } from 'src/app/shared/services/api/settings.api.service';

@Component({
	selector: 'jw-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnInit {
	public user: any = {};
	public setting: any = {};

	constructor(
		private authService: AuthService,
		private usersApiService: UsersApiService,
		private settingsApiService: SettingsApiService,
		private portalService: PortalService,
		private dialog: MatDialog,
		private customNotificationsService: CustomNotificationsService
	) { }

	ngOnInit() {
		this.initUser();
	}

	public onChangePassword() {
		this.dialog.open(UserPasswordComponent, this.prepareUserPassword())
			.afterClosed()
			.subscribe(result => this.afterUserPasswordClose(result));
	}

	public onChangeEmail() {
		this.dialog.open(UserEmailComponent, this.prepareUserEmail())
			.afterClosed()
			.subscribe(result => this.afterUserEmailClose(result));
	}

	public onSaveSetting() {
		this.portalService.showLoader();
		this.settingsApiService.updateSetting(this.setting)
			.subscribe(response => this.updateSettingSuccess(response));
	}

	private initUser() {
		const userId = this.authService.getAuthId();
		this.usersApiService.getUser(userId)
			.subscribe(response => {
				if (response && response.result) {
					this.user = response.result;
					this.setting = this.user.setting;
				}
			});
	}

	private prepareUserPassword() {
		const dialogConfig = this.prepareDialogConfigBase();
		dialogConfig.data = { user: this.user };
		return dialogConfig;
	}

	private prepareUserEmail() {
		const dialogConfig = this.prepareDialogConfigBase();
		dialogConfig.data = { user: this.user };
		return dialogConfig;
	}

	private afterUserEmailClose(result) {
		if (result) {
			this.customNotificationsService.successTranslate('JW.STATEMENT', 'USER.CHANGE_EMAIL_SUCCESS');
			this.authService.logout();
		}
	}

	private afterUserPasswordClose(result) {
		if (result) {
			this.customNotificationsService.successTranslate('JW.STATEMENT', 'USER.CHANGE_PASSWORD_SUCCESS');
			this.authService.logout();
		}
	}

	private updateSettingSuccess(response: any) {
		this.customNotificationsService.successTranslate('JW.STATEMENT', 'SETTINGS.UPDATE_SETTINGS_SUCCESS');
		this.portalService.hideLoader();
	}

	private prepareDialogConfigBase(): MatDialogConfig {
		const dialogConfigBase = new MatDialogConfig();
		dialogConfigBase.autoFocus = true;
		dialogConfigBase.disableClose = true;
		return dialogConfigBase;
	}
}
