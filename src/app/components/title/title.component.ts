import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'jw-title',
	templateUrl: './title.component.html',
	styleUrls: ['./title.component.less']
})
export class TitleComponent implements OnInit {
	@Input() title: string;
	@Input() separator: boolean = false;
	@Input() background: boolean = true;
	@Input() padding: boolean = true;

	constructor() { }

	ngOnInit() {
	}

}
