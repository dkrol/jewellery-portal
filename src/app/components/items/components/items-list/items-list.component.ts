import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'jw-items-list',
	templateUrl: './items-list.component.html',
	styleUrls: ['./items-list.component.less']
})
export class ItemsListComponent implements OnInit {
	@Input() items: Array<any>;

	constructor() { }

	ngOnInit() {
	}
}
