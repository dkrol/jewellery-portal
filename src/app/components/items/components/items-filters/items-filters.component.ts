import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
	selector: 'jw-items-filters',
	templateUrl: './items-filters.component.html',
	styleUrls: ['./items-filters.component.less']
})
export class ItemsFiltersComponent implements OnInit {
	@Output() filtersChanged = new EventEmitter<any>();

	constructor() { }

	ngOnInit() {
	}

	public onFiltersChanged() {
		this.filtersChanged.emit();
	}
}
