import { Component, OnInit, Output, Input, EventEmitter, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { Paginator } from 'src/app/shared/models/Paginator';
import { ImagesService } from 'src/app/shared/services/images/images.service';
import { ItemsService } from 'src/app/shared/services/items/items.service';
import { Router } from '@angular/router';
import { PageInfo } from 'src/app/shared/models/PageInfo';
import { SortInfo } from 'src/app/shared/models/SortInfo';
import { InfoDialogComponent } from 'src/app/components/info-dialog/info-dialog.component';
import { InfoDialogMode } from 'src/app/shared/enums/InfoDialogMode';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';

const DEFAULT_DISPLAYED_COLUMNS: Array<string> =
	['avatar', 'title', 'material', 'weight', 'quantity', 'price', 'creationDate', 'modificationDate', 'isActive', 'actions'];

@Component({
	selector: 'jw-items-table',
	templateUrl: './items-table.component.html',
	styleUrls: ['./items-table.component.less']
})
export class ItemsTableComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	@Input() set itemsData(response: any) { this.setItemsData(response); }
	@Input() isEditAction: boolean;
	@Input() isDeleteAction: boolean;
	@Input() isDeleteFromFavoritesAction: boolean;
	@Input() displayedColumns: Array<string> = DEFAULT_DISPLAYED_COLUMNS;

	@Output() itemsDataUpdate: EventEmitter<any> = new EventEmitter<any>();

	public paginatorConfig: Paginator = Paginator.create();
	public itemsTableDataSource: MatTableDataSource<any>;

	private _searchText: string;
	private _items: Array<any>;

	constructor(
		public imagesService: ImagesService,
		private itemsService: ItemsService,
		private itemsApiService: ItemsApiService,
		private router: Router,
		private dialog: MatDialog) { }

	ngOnInit() {
		this.updateItemsEvent();
	}

	public onItemClick(item: any) {
		this.router.navigate([`item/${item.id}/details`]);
	}

	public onSearchTextChanged(searchText: string) {
		this._searchText = searchText;
		this.updateItemsEvent();
	}

	public onSortChange() {
		this.updateItemsEvent();
	}

	public onPaginatorChange() {
		this.updateItemsEvent();
	}

	public onEdit(event, item: any) {
		event.stopPropagation();
		this.router.navigate([`item/${item.id}/edit`]);
	}

	public onDelete(event, item: any) {
		event.stopPropagation();
		this.dialog.open(InfoDialogComponent, this.prepareDialogConfirmDelete())
			.afterClosed()
			.subscribe(result => this.afterDialogConfirmDeleteClosed(result, item));
	}

	public onDeleteFromFavorites(event, item: any) {
		event.stopPropagation();
		this.itemsService.deleteItemFromFavorites(item)
			.then(() => this.updateItemsEvent());
	}

	private setItemsData(response: any) {
		if (response) {
			this._items = response.results ? response.results : [];
			this.updateTableData();
			this.updatePaginator(response.totalCount);
		}
	}

	private updateTableData() {
		this.itemsTableDataSource = new MatTableDataSource<any>(this._items);
	}

	private updatePaginator(totalCount: number) {
		this.paginatorConfig.length = totalCount;
	}

	private updateItemsEvent() {
		const request = this.prepareRequest();
		this.itemsDataUpdate.emit(request);
	}

	private prepareRequest(): any {
		return {
			pageInfo: PageInfo.Create(this.paginator.pageIndex + 1, this.paginator.pageSize),
			sortInfo: SortInfo.Create(this.sort.active, this.sort.direction),
			searchText: this._searchText
		};
	}

	private afterDialogConfirmDeleteClosed(result: any, item: any) {
		if (result) {
			this.itemsApiService.deleteItem(item.id)
				.subscribe(response => this.updateItemsEvent());
		}
	}

	private prepareDialogConfirmDelete() {
		const dialogConfig = this.prepareDialogConfigBase();
		dialogConfig.data = {
			title: 'ITEM.DELETE_CONFIRM_TITLE',
			text: 'ITEM.DELETE_CONFIRM_TEXT',
			dialogMode: InfoDialogMode.Confirm
		};
		return dialogConfig;
	}

	private prepareDialogConfigBase(): MatDialogConfig {
		const dialogConfigBase = new MatDialogConfig();
		dialogConfigBase.autoFocus = true;
		dialogConfigBase.disableClose = true;
		return dialogConfigBase;
	}
}
