import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { CategoriesApiService } from 'src/app/shared/services/api/categories.api.service';

@Component({
	selector: 'jw-items-categories',
	templateUrl: './items-categories.component.html',
	styleUrls: ['./items-categories.component.less']
})
export class ItemsCategoriesComponent implements OnInit {
	@Input() category: number;
	@Output() categoryChanged = new EventEmitter<number>();

	constructor(public categoriesApiService: CategoriesApiService) { }

	ngOnInit() {
		this.categoriesApiService.updateCategories();
	}

	public onCategory(category) {
		this.categoryChanged.emit(category.id);
	}
}
