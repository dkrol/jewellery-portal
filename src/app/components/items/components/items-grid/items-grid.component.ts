import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'jw-items-grid',
	templateUrl: './items-grid.component.html',
	styleUrls: ['./items-grid.component.less']
})
export class ItemsGridComponent implements OnInit {
	@Input() set size(size: number) { this.initSize(size); }
	@Input() set items(items: Array<any>) { this.initItems(items); }

	public itemsGrid: Array<Array<any>>;
	public itemWidth: number;
	public gridSize: number;

	private _items: Array<any>;

	constructor() { }

	ngOnInit() { }

	private initItems(items: Array<any>) {
		this._items = items;
		if (this._items) {
			this.updateItemsGrid();
		}
	}

	private initSize(size: number) {
		this.gridSize = size;
		this.itemWidth = size;
		if (this._items) {
			this.updateItemsGrid();
		}
	}

	private updateItemsGrid() {
		const itemsCount = this._items.length;
		this.itemsGrid = [];
		let itemsRow = [];

		for (let i = 0, index = 0; i < itemsCount; i++ , index++) {
			if (index !== 0 && index % this.gridSize === 0) {
				this.itemsGrid.push(itemsRow);
				itemsRow = [];
			}

			itemsRow.push(this._items[i]);

			if (i === itemsCount - 1) {
				this.itemsGrid.push(itemsRow);
			}
		}
	}
}
