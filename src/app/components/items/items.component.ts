import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoriesApiService } from 'src/app/shared/services/api/categories.api.service';
import { ActivatedRoute } from '@angular/router';
import { Paginator } from 'src/app/shared/models/Paginator';
import { GetItemsByCategoryIdPageQuery } from 'src/app/shared/domains/items/queries/GetItemsByCategoryIdPageQuery';
import { PageInfo } from 'src/app/shared/models/PageInfo';
import { MatPaginator } from '@angular/material';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { SortInfo } from 'src/app/shared/models/SortInfo';
import { ViewMode } from 'src/app/shared/enums/ViewMode';
import { LocalStorageService } from 'src/app/shared/services/storage/local-storage.service';

@Component({
	selector: 'jw-items',
	templateUrl: './items.component.html',
	styleUrls: ['./items.component.less']
})
export class ItemsComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;

	public readonly itemsViewModeKey = 'jwItemsViewMode';

	public paginatorConfig: Paginator = Paginator.create();
	public selectedCategoryId: number;
	public items: Array<any>;
	public sortInfo: SortInfo;

	public viewMode: ViewMode;
	public viewModeEnum = ViewMode;
	public itemsGridSize = 5;

	private _searchText: string;

	constructor(
		public categoriesApiService: CategoriesApiService,
		private itemsApiService: ItemsApiService,
		private portalService: PortalService,
		private localStorageService: LocalStorageService,
		private activatedRoute: ActivatedRoute) {
		this.selectedCategoryId = activatedRoute.snapshot.params.id;
	}

	ngOnInit() {
		this.initViewMode();
		this.initSortInfo();
		this.updateItems();
	}

	public onSortChanged(sortInfo: SortInfo) {
		this.sortInfo = sortInfo;
		this.updateItems();
	}

	public onSearchTextChanged(searchText: string) {
		this._searchText = searchText;
		this.updateItems();
	}

	public onCategoryChanged(categoryId: number) {
		this.selectedCategoryId = categoryId;
		this.updateItems();
	}

	public onFiltersChanged(filters) {
		console.log(filters);
	}

	public onPaginatorChanged() {
		this.updateItems();
	}

	public onViewModeChanged(viewMode: ViewMode) {
		this.localStorageService.setItem(this.itemsViewModeKey, viewMode);
		this.viewMode = viewMode;
	}

	private updateItems() {
		this.portalService.showLoader();
		const request = this.prepareRequest();
		this.itemsApiService.getItemsByCategoryIdPage(request)
			.subscribe((response: any) => this.getItemsByCategoryIdPageSuccess(response));
	}

	private prepareRequest(): GetItemsByCategoryIdPageQuery {
		const request = new GetItemsByCategoryIdPageQuery();
		request.categoryId = this.selectedCategoryId;
		request.pageInfo = PageInfo.Create(this.paginator.pageIndex + 1, this.paginator.pageSize);
		request.sortInfo = this.sortInfo;
		request.searchText = this._searchText;
		return request;
	}

	private getItemsByCategoryIdPageSuccess(response: any) {
		this.items = response.result && response.result.results ? response.result.results : [];
		this.updatePaginator(response.result.totalCount);
		this.portalService.hideLoader();
	}

	private updatePaginator(totalCount: number) {
		this.paginatorConfig.length = totalCount;
	}

	private initViewMode() {
		const viewMode = this.localStorageService.getItem(this.itemsViewModeKey);
		this.viewMode = viewMode ? viewMode : ViewMode.List;
		this.localStorageService.setItem(this.itemsViewModeKey, this.viewMode);
	}

	private initSortInfo() {
		this.sortInfo = SortInfo.Create('creationDate', 'desc');
	}
}
