import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth/auth-service.service';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { LoginTabEnum } from './enums/login-tab-enum';
import { UserLoginForm } from 'src/app/shared/forms/UserLoginForm';
import { UsersApiService } from 'src/app/shared/services/api/users.api.service';
import { CustomNotificationsService } from 'src/app/shared/services/notifications/custom-notifications.service';
import { AuthApiService } from 'src/app/shared/services/api/auth.api.service';
import { JwtToken } from 'src/app/shared/interfaces/JwtToken';
import { UserRegisterForm } from 'src/app/shared/forms/UserRegistrationForm';
import { UserFormMode } from 'src/app/shared/enums/UserFormMode';

@Component({
	selector: 'jw-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.less'],
	encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
	@ViewChild('tabsElement') tabsElement;

	public loginTabEnum = LoginTabEnum;
	public userFormMode = UserFormMode;
	public authUser: UserLoginForm = new UserLoginForm();
	public userForm: UserRegisterForm = new UserRegisterForm();
	public selectedTabIndex = LoginTabEnum.Signin;

	constructor(
		private usersApiService: UsersApiService,
		private authApiService: AuthApiService,
		private customNotificationService: CustomNotificationsService,
		private portalService: PortalService,
		private authService: AuthService, private router: Router) { }

	ngOnInit() { }

	public loginClick() {
		if (!this.authUser.form.invalid) {
			this.portalService.isLoading = true;
			this.authApiService.authUser(this.authUser.parseJson())
				.subscribe(response => this.authUserSuccess(response));
		}
	}

	public onUserFormSave(userForm: UserRegisterForm) {
		this.userForm = userForm;
		if (!this.userForm.form.invalid) {
			this.portalService.isLoading = true;
			this.usersApiService.registerUser(this.userForm.parseJson())
				.subscribe(() => this.registerUserSuccess());
		}
	}

	public selectedTabChange(event: any) {
		this.updateTabsHeight(event);
	}

	private authUserSuccess(response: JwtToken) {
		this.authService.setAuthToken(response);
		this.portalService.isLoading = false;
		this.router.navigate(['/home']);
	}

	private registerUserSuccess() {
		this.portalService.isLoading = false;
		this.customNotificationService.successTranslate('JW.STATEMENT', 'LOGIN.SIGNUP_SUCCESS');
		this.selectedTabIndex = LoginTabEnum.Signin;
		this.userForm.clear();
	}

	private updateTabsHeight(event: any) {
		if (this.tabsElement && this.tabsElement._elementRef && this.tabsElement._elementRef.nativeElement) {
			const tabs = this.tabsElement._elementRef.nativeElement;
			if (event.index === LoginTabEnum.Signin) {
				this.removeHeightClasses(tabs);
				tabs.classList.add('jw-login-container-tabs-signin--height');
			} else if (event.index === LoginTabEnum.Signup) {
				this.removeHeightClasses(tabs);
				tabs.classList.add('jw-login-container-tabs-signup--height');
			}
		}
	}

	private removeHeightClasses(tabs: any) {
		tabs.classList.remove('jw-login-container-tabs-signup--height');
		tabs.classList.remove('jw-login-container-tabs-signup--height');
	}
}
