import { Component, OnInit } from '@angular/core';
import { MenuNodesService } from 'src/app/shared/services/menu/menu-nodes.service';
import { MenuNode } from 'src/app/shared/models/MenuNode';

@Component({
	selector: 'jw-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

	private _nodesInRow = 5;
	public menuNodeRows: Array<Array<MenuNode>>;

	constructor(public menuNodesService: MenuNodesService) {
		this.initMenuNodeRows();
	}

	ngOnInit() {
		this.updateMenuNodeRows();
		window.onresize = () => this.updateMenuNodeRows();
	}

	private initMenuNodeRows() {
		const nodeCount = this.menuNodesService.dashboardNodes.length;
		this.menuNodeRows = [];
		let nodeRow = [];

		for (let i = 0, index = 0; i < nodeCount; i++ , index++) {
			if (index !== 0 && index % this._nodesInRow === 0) {
				this.menuNodeRows.push(nodeRow);
				nodeRow = [];
			}

			nodeRow.push(this.menuNodesService.dashboardNodes[i]);

			if (i === nodeCount - 1) {
				this.menuNodeRows.push(nodeRow);
			}
		}
	}

	private updateMenuNodeRows() {
		if (window.innerWidth > 1200) {
			this._nodesInRow = 5;
		} else if (window.innerWidth > 850) {
			this._nodesInRow = 3;
		} else if (window.innerWidth > 700) {
			this._nodesInRow = 2;
		} else {
			this._nodesInRow = 1;
		}

		this.initMenuNodeRows();
	}
}
