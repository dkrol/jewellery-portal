import { Component, OnInit } from '@angular/core';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { AuthService } from 'src/app/shared/services/auth/auth-service.service';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from 'src/app/shared/services/storage/local-storage.service';
import { Router } from '@angular/router';

@Component({
	selector: 'jw-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

	private readonly LANGUAGE_KEY = 'jwLanguage';

	constructor(
		public portalService: PortalService,
		public authService: AuthService,
		public translate: TranslateService,
		private localStorageService: LocalStorageService,
		private router: Router) { }

	ngOnInit() {

		// console.log(this.translate);

	 }

	public dashboard() {
		this.router.navigate(['/dashboard']);
	}

	public login() {
		this.router.navigate(['/login']);
	}

	public logout() {
		this.authService.logout();
	}

	public changeLanguage(language: string) {
		this.localStorageService.setItem(this.LANGUAGE_KEY, language);
		this.translate.use(language);
	}

	public getCurrentLanguage() {
		return this.localStorageService.getItem(this.LANGUAGE_KEY);
	}
}
