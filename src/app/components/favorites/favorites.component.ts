import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { ImagesService } from 'src/app/shared/services/images/images.service';

@Component({
	selector: 'jw-favorites',
	templateUrl: './favorites.component.html',
	styleUrls: ['./favorites.component.less']
})
export class FavoritesComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	public readonly displayedColumns: Array<string> = ['avatar', 'title', 'material', 'weight', 'quantity', 'price', 'actions'];
	public itemsData: any;

	constructor(
		public imagesService: ImagesService,
		private portalService: PortalService,
		private itemsApiService: ItemsApiService) { }

	ngOnInit() { }

	public itemsDataUpdate(request: any) {
		this.portalService.showLoader();
		this.itemsApiService.getFavoriteItemsPage(request)
			.subscribe(response => this.getFavoriteItemsPageSuccess(response));
	}

	private getFavoriteItemsPageSuccess(response: any) {
		this.itemsData = response.result;
		this.portalService.hideLoader();
	}
}
