import { Component, OnInit, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'jw-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.less'],
	encapsulation: ViewEncapsulation.None
})
export class SearchComponent implements OnInit {
	@Input() placeholder: string;
	@Output() searchTextChanged = new EventEmitter();

	public searchText: string;

	constructor() { }

	ngOnInit() {
	}

	public onSearchTextChanged() {
		this.searchTextChanged.emit(this.searchText);
	}
}
