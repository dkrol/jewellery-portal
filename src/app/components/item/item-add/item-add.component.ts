import { Component, OnInit } from '@angular/core';
import { ItemForm } from 'src/app/shared/forms/ItemForm';
import { CustomNotificationsService } from 'src/app/shared/services/notifications/custom-notifications.service';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';
import { ImagesApiService } from 'src/app/shared/services/api/images.api.service';
import { Router } from '@angular/router';
import { PortalService } from 'src/app/shared/services/portal/portal.service';

@Component({
	selector: 'jw-item-add',
	templateUrl: './item-add.component.html',
	styleUrls: ['./item-add.component.less']
})
export class ItemAddComponent implements OnInit {

	public itemForm = new ItemForm();

	constructor(
		private customNotificationService: CustomNotificationsService,
		private itemsApiService: ItemsApiService,
		private imagesApiService: ImagesApiService,
		private router: Router,
		private portalService: PortalService) { }

	ngOnInit() {
	}

	public onItemFormSave(itemForm: ItemForm) {
		this.portalService.showLoader();
		this.imagesApiService.uploadImages(itemForm.getImagesFormData())
			.subscribe(response => this.uploadImagesSuccess(response, itemForm));
	}

	private uploadImagesSuccess(response: any, itemForm: ItemForm) {
		if (response && response.result) {
			if (response.result.length > 0) {
				const itemData = itemForm.parseJsonToAdd(response.result);
				this.addItem(itemData);
			} else {
				this.customNotificationService.errorTranslate('JW.ERROR', 'ITEM.UPLOAD_IMAGES_ERROR');
				this.portalService.hideLoader();
			}
		} else {
			this.portalService.hideLoader();
		}
	}

	private addItem(itemData: any) {
		this.itemsApiService.addItem(itemData)
			.subscribe(response => this.addItemSuccess(response));
	}

	private addItemSuccess(response: any) {
		this.portalService.hideLoader();
		this.customNotificationService.successTranslate('JW.STATEMENT', 'JW.SAVE_SUCCESS');
		this.router.navigate(['/']);
	}
}
