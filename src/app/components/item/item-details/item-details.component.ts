import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { ImagesService } from 'src/app/shared/services/images/images.service';

@Component({
	selector: 'jw-item-details',
	templateUrl: './item-details.component.html',
	styleUrls: ['./item-details.component.less'],
	encapsulation: ViewEncapsulation.None
})
export class ItemDetailsComponent implements OnInit {
	public galleryOptions: NgxGalleryOptions[] = [];
	public galleryImages: NgxGalleryImage[] = [];
	private item: any = {};

	private _itemId: number;
	private _itemImageUrls: Array<string> = [];

	constructor(
		private activatedRoute: ActivatedRoute,
		private itemsApiService: ItemsApiService,
		private portalService: PortalService,
		private imagesService: ImagesService) {
		this._itemId = activatedRoute.snapshot.params.id;
	}

	ngOnInit() {
		this.initItem();
	}

	private initItem() {
		this.portalService.showLoader();
		this.itemsApiService.getItemById(this._itemId)
			.subscribe(response => this.getItemByIdSuccess(response));
	}

	private getItemByIdSuccess(response: any) {
		this.item = response && response.result ? response.result : {};
		this.initItemImageUrls();
		this.initGallery();
		this.portalService.hideLoader();
	}

	private initItemImageUrls() {
		this._itemImageUrls = this.imagesService.getImageUrls(this.item.images.map(image => image));
	}

	private initGallery() {
		this.galleryOptions = this.getGalleryOptions();
		this.galleryImages = this._itemImageUrls.map(url =>
			({ small: url, medium: url, big: url })
		);
	}

	private getGalleryOptions() {
		return [
			{
				previewCloseOnClick: true,
				previewCloseOnEsc: true,
				previewZoom: true,
				previewRotate: true,
				imageArrowsAutoHide: true,
				thumbnailsArrowsAutoHide: true,
				spinnerIcon: 'fa fa-circle-o-notch fa-spin fa-3x fa-fw',
			},
			{
				width: '100%',
				height: '40vw',
				thumbnailsColumns: 4,
				imageAnimation: NgxGalleryAnimation.Slide,
				thumbnailsMargin: 5,
			},
			{
				breakpoint: 1000,
				thumbnailsColumns: 2,
				height: '50vw',
			}
		];
	}
}
