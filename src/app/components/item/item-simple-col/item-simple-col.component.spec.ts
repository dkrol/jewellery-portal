import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSimpleColComponent } from './item-simple-col.component';

describe('ItemSimpleColComponent', () => {
  let component: ItemSimpleColComponent;
  let fixture: ComponentFixture<ItemSimpleColComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSimpleColComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSimpleColComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
