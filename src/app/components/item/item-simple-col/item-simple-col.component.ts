import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ImagesService } from 'src/app/shared/services/images/images.service';
import { ItemsService } from 'src/app/shared/services/items/items.service';

@Component({
	selector: 'jw-item-simple-col',
	templateUrl: './item-simple-col.component.html',
	styleUrls: ['./item-simple-col.component.less']
})
export class ItemSimpleColComponent implements OnInit {
	@Input() item: any;

	constructor(
		public imagesService: ImagesService,
		public itemsService: ItemsService,
		private router: Router) { }

	ngOnInit() {
	}

	public onItem() {
		this.router.navigate([`item/${this.item.id}/details`]);
	}
}
