import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSimpleRowComponent } from './item-simple-row.component';

describe('ItemSimpleRowComponent', () => {
  let component: ItemSimpleRowComponent;
  let fixture: ComponentFixture<ItemSimpleRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSimpleRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSimpleRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
