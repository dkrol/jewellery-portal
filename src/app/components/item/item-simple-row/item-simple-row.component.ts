import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ImagesService } from 'src/app/shared/services/images/images.service';
import { ItemsService } from 'src/app/shared/services/items/items.service';

@Component({
	selector: 'jw-item-simple-row',
	templateUrl: './item-simple-row.component.html',
	styleUrls: ['./item-simple-row.component.less']
})
export class ItemSimpleRowComponent implements OnInit {
	@Input() item: any;

	constructor(
		public imagesService: ImagesService,
		public itemsService: ItemsService,
		private router: Router) { }

	ngOnInit() {
	}

	public onItem() {
		this.router.navigate([`item/${this.item.id}/details`]);
	}
}
