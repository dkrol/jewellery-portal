import { Component, OnInit } from '@angular/core';
import { ItemForm } from 'src/app/shared/forms/ItemForm';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemsApiService } from 'src/app/shared/services/api/items.api.service';
import { PortalService } from 'src/app/shared/services/portal/portal.service';
import { ImagesApiService } from 'src/app/shared/services/api/images.api.service';
import { CustomNotificationsService } from 'src/app/shared/services/notifications/custom-notifications.service';
import { PreviousRouteService } from 'src/app/shared/services/router/previous-route.service';

@Component({
	selector: 'jw-item-edit',
	templateUrl: './item-edit.component.html',
	styleUrls: ['./item-edit.component.less']
})
export class ItemEditComponent implements OnInit {
	public itemForm = new ItemForm();
	private itemId: number;

	constructor(
		activatedRoute: ActivatedRoute,
		private itemsApiService: ItemsApiService,
		private portalService: PortalService,
		private imagesApiService: ImagesApiService,
		private customNotificationService: CustomNotificationsService,
		private previousRouteService: PreviousRouteService,
		private router: Router) {
		this.itemId = activatedRoute.snapshot.params.id;
	}

	ngOnInit() {
		this.initItem();
	}

	public onItemFormSave(itemForm: ItemForm) {
		this.portalService.showLoader();
		this.imagesApiService.uploadImages(itemForm.getImagesFormData())
			.subscribe((response: any) => this.uploadImagesSuccess(response, itemForm));
	}

	private uploadImagesSuccess(response: any, itemForm: ItemForm) {
		if (response && response.result) {
			const itemData = itemForm.parseJsonToUpdate(response.result);
			this.updateItem(itemData);
		} else {
			this.portalService.hideLoader();
		}
	}

	private updateItem(itemData: any) {
		itemData.id = this.itemId;
		this.itemsApiService.updateItem(itemData)
			.subscribe(response => this.updateItemSuccess(response));
	}

	private updateItemSuccess(response: any) {
		this.portalService.hideLoader();
		this.customNotificationService.successTranslate('JW.STATEMENT', 'JW.SAVE_SUCCESS');
		this.router.navigate([this.previousRouteService.previousUrl]);
	}

	private initItem() {
		this.itemsApiService.getItemById(this.itemId)
			.subscribe(response => this.getItemByIdSuccess(response));
	}

	private getItemByIdSuccess(response: any) {
		this.itemForm.loadData(response.result);
	}
}
