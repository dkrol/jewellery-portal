import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemForm } from 'src/app/shared/forms/ItemForm';
import { CategoriesApiService } from 'src/app/shared/services/api/categories.api.service';
import { CustomNotificationsService } from 'src/app/shared/services/notifications/custom-notifications.service';
import * as _ from 'lodash';

@Component({
	selector: 'jw-item-form',
	templateUrl: './item-form.component.html',
	styleUrls: ['./item-form.component.less']
})
export class ItemFormComponent implements OnInit {
	@Output('onSave') onSaveChanged: EventEmitter<ItemForm> = new EventEmitter();
	@Input('itemForm') item: ItemForm;

	public imageDeleteVisible = [];
	public categories = [];
	public dropzoneActive = false;

	constructor(
		private customNotificationService: CustomNotificationsService,
		private categoriesApiService: CategoriesApiService) { }

	ngOnInit() {
		this.initCategories();
	}

	public saveItem() {
		this.onSaveChanged.emit(this.item);
	}

	public onFilesDroppped(fileList: FileList) {
		const images = this.getImageFiles(fileList);
		this.item.addImages(images);
	}

	public onFilesHovered(active: boolean) {
		this.dropzoneActive = active;
	}

	public onFilesSelected(event) {
		const images = this.getImageFiles(event.target.files);
		this.item.addImages(images);
		event.srcElement.value = null;
	}

	public deleteImage(image) {
		this.imageDeleteVisible = [];
		this.item.deleteImage(image);
	}

	private initCategories() {
		this.categoriesApiService.getCategories()
			.subscribe(response => this.categories = response.result ? response.result : []);
	}

	private getImageFiles(fileList: FileList): Array<File> {
		const fileArray = Array.from(fileList);

		for (let i = fileArray.length - 1; i >= 0; i--) {
			if (!this.isImageValid(fileArray[i])) {
				this.customNotificationService.errorTranslateMany('JW.ERROR', ['ITEM.IMAGE_FORMAT_INVALID', fileArray[i].name]);
				const index = Array.from(fileList).indexOf(fileArray[i]);
				fileArray.splice(index, 1);
			}
		}

		return fileArray;
	}

	private isImageValid(file: File): boolean {
		const imageExtensions = /(\.jpg|\.jpeg|\.bmp|\.gif|\.png)$/i;
		return imageExtensions.exec(file.name) ? true : false;
	}
}
