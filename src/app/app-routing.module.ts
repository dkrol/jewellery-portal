import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ItemsComponent } from './components/items/items.component';
import { ErrorComponent } from './components/error/error.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { GuestGuard } from './shared/guards/guest.guard';
import { OrdersComponent } from './components/orders/orders.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AuthAdminGuard } from './shared/guards/auth.admin.guard';
import { AuthCustomerGuard } from './shared/guards/auth.customer.guard';
import { ItemEditComponent } from './components/item/item-edit/item-edit.component';
import { ItemAddComponent } from './components/item/item-add/item-add.component';
import { ItemDetailsComponent } from './components/item/item-details/item-details.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { ManageItemsComponent } from './components/manage-items/manage-items.component';
import { ManageUsersComponent } from './components/manage-users/manage-users.component';
import { RegulationsComponent } from './components/regulations/regulations.component';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent,
		pathMatch: 'full'
	},
	{
		path: 'home',
		component: HomeComponent
	},
	{
		path: 'login',
		component: LoginComponent,
		canActivate: [GuestGuard]
	},
	{
		path: 'favorites',
		component: FavoritesComponent,
		canActivate: [AuthGuard],
	},
	{
		path: 'orders',
		component: OrdersComponent,
		canActivate: [AuthGuard, AuthCustomerGuard]
	},
	{
		path: 'settings',
		component: SettingsComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'item/add',
		component: ItemAddComponent,
		pathMatch: 'full',
		canActivate: [AuthGuard, AuthAdminGuard]
	},
	{
		path: 'item/:id/details',
		component: ItemDetailsComponent,
		pathMatch: 'full',
	},
	{
		path: 'item/:id/edit',
		component: ItemEditComponent,
		pathMatch: 'full',
		canActivate: [AuthGuard, AuthAdminGuard]
	},
	{
		path: 'manage/items',
		component: ManageItemsComponent,
		pathMatch: 'full',
		canActivate: [AuthGuard, AuthAdminGuard]
	},
	{
		path: 'manage/users',
		component: ManageUsersComponent,
		pathMatch: 'full',
		canActivate: [AuthGuard, AuthAdminGuard]
	},
	{
		path: 'categories/:id/items',
		component: ItemsComponent
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'regulations',
		component: RegulationsComponent
	},
	{
		path: '**',
		component: ErrorComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
