import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpConfigInterceptor } from './shared/interceptors/httpconfig.interceptor';
import { SimpleNotificationsModule, NotificationAnimationType } from 'angular2-notifications';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxGalleryModule } from 'ngx-gallery';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { translateLoaderFactory } from './shared/services/translate/translate.helper';
import { MaterialModule } from './shared/material.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { BannerComponent } from './components/banner/banner.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { ItemsComponent } from './components/items/items.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { ErrorComponent } from './components/error/error.component';
import { LoaderComponent } from './components/loader/loader.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SettingsComponent } from './components/settings/settings.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ItemsFiltersComponent } from './components/items/components/items-filters/items-filters.component';
import { ItemsCategoriesComponent } from './components/items/components/items-categories/items-categories.component';
import { ItemsListComponent } from './components/items/components/items-list/items-list.component';
import { SearchComponent } from './components/search/search.component';
import { MatPaginatorIntl } from '@angular/material';
import { PaginatorI18n } from './shared/services/paginator/PaginatorI18n';
import { ItemFormComponent } from './components/item/item-form/item-form.component';
import { ItemAddComponent } from './components/item/item-add/item-add.component';
import { ItemEditComponent } from './components/item/item-edit/item-edit.component';
import { FileDropDirective } from './shared/directives/file-drop/file-drop.directive';
import { ItemDetailsComponent } from './components/item/item-details/item-details.component';
import { SortComponent } from './components/sort/sort.component';
import { ItemSimpleRowComponent } from './components/item/item-simple-row/item-simple-row.component';
import { ItemSimpleColComponent } from './components/item/item-simple-col/item-simple-col.component';
import { HeightByWidthDirective } from './shared/directives/height-by-width/height-by-width.directive';
import { ItemsGridComponent } from './components/items/components/items-grid/items-grid.component';
import { SeparatorComponent } from './components/separator/separator.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { FavoriteComponent } from './components/favorite/favorite.component';
import { ManageItemsComponent } from './components/manage-items/manage-items.component';
import { ManageUsersComponent } from './components/manage-users/manage-users.component';
import { ItemsTableComponent } from './components/items/components/items-table/items-table.component';
import { ManageItemsActiveComponent } from './components/manage-items/components/manage-items-active/manage-items-active.component';
import { ManageItemsInactiveComponent } from './components/manage-items/components/manage-items-inactive/manage-items-inactive.component';
import { ManageItemsSoldComponent } from './components/manage-items/components/manage-items-sold/manage-items-sold.component';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { ShowHoverDirective } from './shared/directives/show-hover/show-hover.directive';
import { ManageUsersActiveComponent } from './components/manage-users/components/manage-users-active/manage-users-active.component';
import { ManageUsersInactiveComponent } from './components/manage-users/components/manage-users-inactive/manage-users-inactive.component';
import { ManageUsersAllComponent } from './components/manage-users/components/manage-users-all/manage-users-all.component';
import { UsersTableComponent } from './components/users/users-table/users-table.component';
import { UserPasswordComponent } from './components/user/user-password/user-password.component';
import { UserEmailComponent } from './components/user/user-email/user-email.component';
import { QuantityComponent } from './components/quantity/quantity.component';
import { RegulationsComponent } from './components/regulations/regulations.component';
import { UserRegistrationComponent } from './components/user/user-registration/user-registration.component';
import { UserEditComponent } from './components/user/user-edit/user-edit.component';
import { TitleComponent } from './components/title/title.component';

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		HeaderComponent,
		FooterComponent,
		HomeComponent,
		BannerComponent,
		MenuBarComponent,
		ItemsComponent,
		SidenavComponent,
		ErrorComponent,
		LoaderComponent,
		DashboardComponent,
		SettingsComponent,
		OrdersComponent,
		ItemsFiltersComponent,
		ItemsCategoriesComponent,
		ItemsListComponent,
		SearchComponent,
		ItemFormComponent,
		ItemAddComponent,
		ItemEditComponent,
		FileDropDirective,
		ItemDetailsComponent,
		SortComponent,
		ItemSimpleRowComponent,
		ItemSimpleColComponent,
		HeightByWidthDirective,
		ItemsGridComponent,
		SeparatorComponent,
		FavoritesComponent,
		FavoriteComponent,
		ManageItemsComponent,
		ManageUsersComponent,
		ItemsTableComponent,
		ManageItemsActiveComponent,
		ManageItemsInactiveComponent,
		ManageItemsSoldComponent,
		InfoDialogComponent,
		ShowHoverDirective,
		UserRegistrationComponent,
		UserEditComponent,
		ManageUsersActiveComponent,
		ManageUsersInactiveComponent,
		ManageUsersAllComponent,
		UsersTableComponent,
		UserPasswordComponent,
		UserEmailComponent,
		QuantityComponent,
		RegulationsComponent,
		TitleComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		Angular2FontawesomeModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		MaterialModule,
		NgxGalleryModule,
		SimpleNotificationsModule.forRoot({
			timeOut: 5000,
			showProgressBar: true,
			pauseOnHover: true,
			animate: NotificationAnimationType.FromRight
		}),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: translateLoaderFactory,
				deps: [HttpClient]
			}
		})
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: HttpConfigInterceptor,
			multi: true
		},
		{
			provide: MatPaginatorIntl,
			useClass: PaginatorI18n
		}
	],
	bootstrap: [AppComponent],
	entryComponents: [
		InfoDialogComponent,
		UserPasswordComponent,
		UserEmailComponent,
		UserEditComponent
	]
})
export class AppModule { }
