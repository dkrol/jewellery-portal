import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UpdateSettingCommand } from '../../domains/settings/commands/UpdateSettingCommand';

@Injectable({
	providedIn: 'root'
})
export class SettingsApiService {

	constructor(private http: HttpClient) { }

	public updateSetting(command: UpdateSettingCommand): Observable<any> {
		return this.http.put<any>('/api/settings', command);
	}
}
