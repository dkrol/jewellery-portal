import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class CategoriesApiService {

	private _categories: any = {};

	public get categories() {
		if (this._categories.length === 0) {
			this.updateCategories();
		}

		return this._categories;
	}

	constructor(private http: HttpClient) {
		this.updateCategories();
	}

	public getCategories(): Observable<any> {
		return new Observable(observer => {
			this.http.get('/api/categories')
				.subscribe((response: any) => {
					this._categories.data = response.result ? response.result : [];
					observer.next(response);
				});
		});
	}

	public updateCategories() {
		this.http.get('/api/categories')
			.subscribe((response: any) => this._categories.data = response.result ? response.result : []);
	}
}
