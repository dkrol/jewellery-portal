import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AddUserCommand } from '../../domains/users/commands/AddUserCommand';
import { ParseService } from '../parse/parse.service';
import { GetUsersFilterPageQuery } from '../../domains/users/queries/GetUsersFilterPageQuery';
import { ChangeUserPasswordCommand } from '../../domains/users/commands/ChangeUserPasswordCommand';
import { UpdateUserCommand } from '../../domains/users/commands/UpdateUserCommand';
import { ChangeUserEmailCommand } from '../../domains/users/commands/ChangeUserEmailCommand';

@Injectable({
	providedIn: 'root'
})
export class UsersApiService {

	constructor(
		private http: HttpClient,
		private parseService: ParseService) { }

	public getUser(id: number) {
		return this.http.get<any>(`/api/users/${id}`);
	}

	public registerUser(command: AddUserCommand): Observable<any> {
		return this.http.post<AddUserCommand>('/api/users', command);
	}

	public getUsersFilterPage(query: GetUsersFilterPageQuery): Observable<any> {
		const params = this.parseService.toHttpParams(query);
		return this.http.get<any>(`/api/users/filter/page`, { params });
	}

	public changeCurrentUserPassword(password: string): Observable<any> {
		return this.http.put<any>(`/api/users/password`, { password });
	}

	public changeUserPassword(command: ChangeUserPasswordCommand): Observable<any> {
		return this.http.put<any>(`/api/users/${command.id}/password`,  command);
	}

	public changeUserEmail(command: ChangeUserEmailCommand): Observable<any> {
		return this.http.put<any>(`/api/users/${command.id}/email`,  command);
	}

	public updateUser(command: UpdateUserCommand) {
		return this.http.put<any>(`/api/users`,  command);
	}
}
