import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParseService } from '../parse/parse.service';

@Injectable({
	providedIn: 'root'
})
export class RolesApiService {

	constructor(
		private http: HttpClient,
		private parseService: ParseService) { }

	public getRoles() {
		return this.http.get<any>(`/api/roles`);
	}
}
