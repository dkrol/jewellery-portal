import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtToken } from '../../interfaces/JwtToken';

@Injectable({
	providedIn: 'root'
})
export class AuthApiService {

	constructor(private http: HttpClient) { }

	public authUser(authUser: any): Observable<JwtToken> {
		return this.http.post<JwtToken>('/api/auth', authUser);
	}
}
