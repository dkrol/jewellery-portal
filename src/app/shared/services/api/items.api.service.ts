import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AddItemCommand } from '../../domains/items/commands/AddItemCommand';
import { UpdateItemCommand } from '../../domains/items/commands/UpdateItemCommand';
import { GetItemsPageQuery } from '../../domains/items/queries/GetItemsPageQuery';
import { ParseService } from '../parse/parse.service';
import { GetItemsByCategoryIdPageQuery } from '../../domains/items/queries/GetItemsByCategoryIdPageQuery';
import { GetFavoriteItemsPageQuery } from '../../domains/items/queries/GetFavoriteItemsPageQuery';
import { GetItemsFilterPageQuery } from '../../domains/items/queries/GetItemsFilterPageQuery';

@Injectable({
	providedIn: 'root'
})
export class ItemsApiService {
	constructor(private http: HttpClient, private parseService: ParseService) { }

	public getItemById(itemId: number) {
		return this.http.get<any>(`/api/items/${itemId}`);
	}

	public addItem(command: AddItemCommand): Observable<any> {
		return this.http.post<any>('/api/items', command);
	}

	public updateItem(command: UpdateItemCommand): Observable<any> {
		return this.http.put<any>('/api/items', command);
	}

	public getItemsPage(query: GetItemsPageQuery): Observable<any> {
		const params = this.parseService.toHttpParams(query);
		return this.http.get<any>('/api/items/page', { params });
	}

	public getItemsFilterPage(query: GetItemsFilterPageQuery): Observable<any> {
		const params = this.parseService.toHttpParams(query);
		return this.http.get<any>('/api/items/filter/page', { params });
	}

	public getItemsByCategoryIdPage(query: GetItemsByCategoryIdPageQuery): Observable<any> {
		const params = this.parseService.toHttpParams(query);
		return this.http.get<any>(`/api/categories/${query.categoryId}/items/page`, { params });
	}

	public deleteItem(itemId: number) {
		return this.http.delete<any>(`/api/items/${itemId}`);
	}

	public addItemToFavorites(itemId: number): Observable<any> {
		return this.http.post<any>(`/api/items/${itemId}/favorite`, {});
	}

	public deleteItemFromFavorites(itemId: number): Observable<any> {
		return this.http.delete<any>(`/api/items/${itemId}/favorite`);
	}

	public getFavoriteItemsPage(query: GetFavoriteItemsPageQuery): Observable<any> {
		const params = this.parseService.toHttpParams(query);
		return this.http.get<any>('/api/favorites/page', { params });
	}
}
