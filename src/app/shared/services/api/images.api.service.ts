import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AddItemCommand } from '../../domains/items/commands/AddItemCommand';

@Injectable({
	providedIn: 'root'
})
export class ImagesApiService {

	constructor(private http: HttpClient) { }

	public uploadImages(formData: FormData): Observable<any> {
		return this.http.post<any>('/api/images', formData);
	}
}
