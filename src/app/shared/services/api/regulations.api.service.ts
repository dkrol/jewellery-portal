import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
	providedIn: 'root'
})
export class RegulationsApiService {

	constructor(
		private http: HttpClient,
		private translate: TranslateService) { }

	public getRegulations() {
		const link = `assets/files/regulations/${this.translate.currentLang}.txt`;
		return this.http.get(link, {responseType: 'text'});
	}
}
