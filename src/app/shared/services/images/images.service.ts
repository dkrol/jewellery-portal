import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class ImagesService {

	constructor() { }

	public getImageUrl(name: string): string {
		return `${window.location.origin}/images/${name}`;
	}

	public getImageUrls(names: Array<string>) {
		return names.map(name => this.getImageUrl(name));
	}

	public getImageUrlCss(name: string): string {
		return `url('${this.getImageUrl(name)}')`;
	}
}
