import { Injectable } from '@angular/core';
import { ItemsApiService } from '../api/items.api.service';
import { AuthService } from '../auth/auth-service.service';
import { CustomNotificationsService } from '../notifications/custom-notifications.service';
import { PortalService } from '../portal/portal.service';

@Injectable({
	providedIn: 'root'
})
export class ItemsService {
	constructor(
		private portalService: PortalService,
		private authService: AuthService,
		private itemsApiService: ItemsApiService,
		private customNotificationsService: CustomNotificationsService) { }

	public addItemToFavorites(item: any): Promise<any> {
		return new Promise((resolve, reject) => {
			if (this.isLogged()) {
				this.portalService.showLoader();
				this.itemsApiService.addItemToFavorites(item.id)
					.subscribe(response => this.addItemToFavoritesSuccess(response, item, resolve));
			} else {
				reject();
			}
		});
	}

	public deleteItemFromFavorites(item: any): Promise<any> {
		return new Promise((resolve, reject) => {
			if (this.isLogged()) {
				this.portalService.showLoader();
				this.itemsApiService.deleteItemFromFavorites(item.id)
					.subscribe(response => this.deleteItemFromFavoritesSuccess(response, item, resolve));
			} else {
				reject();
			}
		});
	}

	private isLogged() {
		if (!this.authService.isLogged) {
			this.customNotificationsService.errorTranslate('JW.ERROR', 'ITEM.ADD_ITEM_TO_FAVORITES_ERROR');
		}
		return true;
	}

	private addItemToFavoritesSuccess(response, item, resolve) {
		this.customNotificationsService.successTranslate('JW.STATEMENT', 'ITEM.ADD_ITEM_TO_FAVORITES_SUCCESS');
		item.isFavorite = true;
		this.portalService.hideLoader();
		resolve();
	}

	private deleteItemFromFavoritesSuccess(response, item, resolve) {
		this.customNotificationsService.successTranslate('JW.STATEMENT', 'ITEM.DELETE_ITEM_FROM_FAVORITES_SUCCESS');
		item.isFavorite = false;
		this.portalService.hideLoader();
		resolve();
	}
}
