import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class ParseService {

	constructor() { }

	public toHttpParams(object: any) {
		let params = new HttpParams();
		const flatObject = this.toFlat(object);

		for (const prop in flatObject) {
			if (flatObject.hasOwnProperty(prop) && flatObject[prop] !== undefined && flatObject[prop] !== null) {
				params = params.append(prop, flatObject[prop]);
			}
		}

		return params;
	}

	public toFlat(object: any) {
		const toReturn = {};
		for (const i in object) {
			if (!object.hasOwnProperty(i)) {
				continue;
			}

			if ((typeof object[i]) === 'object' && object[i] !== null) {
				const flatObject = this.toFlat(object[i]);

				for (const x in flatObject) {
					if (!flatObject.hasOwnProperty(x)) {
						continue;
					}

					toReturn[`${i}.${x}`] = flatObject[x];
				}
			} else {
				toReturn[i] = object[i];
			}
		}

		return toReturn;
	}
}
