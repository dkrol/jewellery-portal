import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Injectable({
	providedIn: 'root'
})
export class PortalService {

	public set sidenav(value: MatSidenav) { this._sidenav = value; }
	public isLoading = false;

	private _sidenav: MatSidenav;

	constructor() { }

	public showLoader() {
		setTimeout(() => this.isLoading = true);
	}

	public hideLoader() {
		setTimeout(() => this.isLoading = false);
	}

	public menuToggle() {
		this._sidenav.toggle();
	}

	public showMenu() {
		this._sidenav.opened = true;
	}

	public hideMenu() {
		this._sidenav.opened = false;
	}
}
