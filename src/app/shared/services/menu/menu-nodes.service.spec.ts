import { TestBed } from '@angular/core/testing';

import { MenuNodesService } from './menu-nodes.service';

describe('MenuNodesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuNodesService = TestBed.get(MenuNodesService);
    expect(service).toBeTruthy();
  });
});
