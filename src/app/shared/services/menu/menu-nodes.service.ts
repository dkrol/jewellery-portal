import { Injectable } from '@angular/core';
import { MenuNode } from '../../models/MenuNode';
import { AuthService } from '../auth/auth-service.service';

@Injectable({
	providedIn: 'root'
})
export class MenuNodesService {

	public get sidenavNodes(): Array<MenuNode> {
		if (this.authService.isAdminRole) {
			return this.menuSidenavNodesForAdmin();
		} else if (this.authService.isCustomerRole) {
			return this.menuSidenavNodesForCustomer();
		}

		return [];
	}

	public get dashboardNodes(): Array<MenuNode> {
		if (this.authService.isAdminRole) {
			return this.menuDashboardNodesForAdmin();
		} else if (this.authService.isCustomerRole) {
			return this.menuDashboardNodesForCustomer();
		}

		return [];
	}

	private _nodes: Array<MenuNode>;

	constructor(private authService: AuthService) { }

	private menuSidenavNodesForAdmin() {
		return [
			new MenuNode('MENU.HOME', '/home', 'home'),
			new MenuNode('MENU.DASHBOARD', '/dashboard', 'dashboard'),
			new MenuNode('MENU.MESSAGES', '/messages', 'message'),
			new MenuNode('MENU.FAVORITES', '/favorites', 'favorite'),
			new MenuNode('MENU.CART', '/cart', 'shopping_cart'),
			new MenuNode('MENU.ORDERS', '/orders', 'shopping_basket'),
			new MenuNode('MENU.ADD_ITEM', '/item/add', 'add_circle'),
			new MenuNode('MENU.ITEMS', '/manage/items', 'work'),
			new MenuNode('MENU.USERS', '/manage/users', 'supervised_user_circle'),
			new MenuNode('MENU.SETTINGS', '/settings', 'settings')
		];
	}

	private menuSidenavNodesForCustomer() {
		return [
			new MenuNode('MENU.HOME', '/home', 'home'),
			new MenuNode('MENU.DASHBOARD', '/dashboard', 'dashboard'),
			new MenuNode('MENU.MESSAGES', '/messages', 'message'),
			new MenuNode('MENU.FAVORITES', '/favorites', 'favorite'),
			new MenuNode('MENU.CART', '/cart', 'shopping_cart'),
			new MenuNode('MENU.ORDERS', '/orders', 'shopping_basket'),
			new MenuNode('MENU.SETTINGS', '/settings', 'settings')
		];
	}

	private menuDashboardNodesForAdmin(): Array<MenuNode> {
		return [
			new MenuNode('MENU.HOME', '/home', 'home'),
			new MenuNode('MENU.MESSAGES', '/messages', 'message'),
			new MenuNode('MENU.FAVORITES', '/favorites', 'favorite'),
			new MenuNode('MENU.CART', '/cart', 'shopping_cart'),
			new MenuNode('MENU.ORDERS', '/orders', 'shopping_basket'),
			new MenuNode('MENU.ADD_ITEM', '/item/add', 'add_circle'),
			new MenuNode('MENU.ITEMS', '/manage/items', 'work'),
			new MenuNode('MENU.USERS', '/manage/users', 'supervised_user_circle'),
			new MenuNode('MENU.SETTINGS', '/settings', 'settings')
		];
	}

	private menuDashboardNodesForCustomer(): Array<MenuNode> {
		return [
			new MenuNode('MENU.HOME', '/home', 'home'),
			new MenuNode('MENU.MESSAGES', '/messages', 'message'),
			new MenuNode('MENU.FAVORITES', '/favorites', 'favorite'),
			new MenuNode('MENU.CART', '/cart', 'shopping_cart'),
			new MenuNode('MENU.ORDERS', '/orders', 'shopping_basket'),
			new MenuNode('MENU.SETTINGS', '/settings', 'settings')
		];
	}
}
