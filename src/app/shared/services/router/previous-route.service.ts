import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class PreviousRouteService {
	public get previousUrl() {
		return this._previousUrl;
	}

	private _previousUrl: string;
	private _currentUrl: string;

	constructor(private router: Router) {
		this.initUrls();
	}

	private initUrls() {
		this._currentUrl = this.router.url;
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				this._previousUrl = this._currentUrl;
				this._currentUrl = event.url;
			}
		});
	}
}
