import { MatPaginatorIntl } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable()
export class PaginatorI18n extends MatPaginatorIntl {

	private readonly PAGINATOR_TRANSLATE_CODES =
		['PAGINATOR.ITEMS_PER_PAGE_LABEL', 'PAGINATOR.NEXT_PAGE_LABEL',
			'PAGINATOR.PREVIOUS_PAGE_LABEL', 'PAGINATOR.FIRST_PAGE_LABEL',
			'PAGINATOR.LAST_PAGE_LABEL'];

	constructor(private translate: TranslateService) {
		super();
		this.translate.onLangChange.subscribe(() => this.getPaginatorIntl());
		this.getPaginatorIntl();
	}

	getPaginatorIntl() {
		this.translate.get(this.PAGINATOR_TRANSLATE_CODES).subscribe(translation => {
			this.itemsPerPageLabel = translation[this.PAGINATOR_TRANSLATE_CODES[0]];
			this.nextPageLabel = translation[this.PAGINATOR_TRANSLATE_CODES[1]];
			this.previousPageLabel = translation[this.PAGINATOR_TRANSLATE_CODES[2]];
			this.firstPageLabel = translation[this.PAGINATOR_TRANSLATE_CODES[3]];
			this.lastPageLabel = translation[this.PAGINATOR_TRANSLATE_CODES[4]];
			this.changes.next();
		});
	}

	getRangeLabel = (page: number, pageSize: number, length: number): string => {
		if (length === 0 || pageSize === 0) {
			return `0 ${this.translate.instant('PAGINATOR.OF_LABEL')} ${length}`;
		}

		length = Math.max(length, 0);
		const startIndex = page * pageSize;
		const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

		return `${startIndex + 1} - ${endIndex} ${this.translate.instant('PAGINATOR.OF_LABEL')}  ${length}`
	}
}
