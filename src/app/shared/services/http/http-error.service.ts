import { Injectable } from '@angular/core';
import { CustomNotificationsService } from '../notifications/custom-notifications.service';

@Injectable({
	providedIn: 'root'
})
export class HttpErrorService {

	constructor(private customNotificationsService: CustomNotificationsService) { }

	public notifyUser(httpErrorResponse: any) {
		if (httpErrorResponse.status === 400 && httpErrorResponse.error) {
			const translationKeys = this.getTranslationKeys(httpErrorResponse.error.errors)
				.map(key => `VALIDATIONS.${key}`);
			this.customNotificationsService.errorTranslateMany('JW.ERROR', translationKeys);
		} else if (httpErrorResponse.status === 500) {
			this.customNotificationsService.errorTranslate('JW.ERROR', 'VALIDATIONS.INTERNAL_SERVER_ERROR');
		} else if (httpErrorResponse.status === 502 || httpErrorResponse.status === 503 || httpErrorResponse.status === 504) {
			this.customNotificationsService.errorTranslate('JW.ERROR', 'VALIDATIONS.SERVER_UNAVAILABLE');
		}
	}

	private getTranslationKeys(errors): Array<any> {
		const result = [];
		if (errors) {
			for (const key in errors) {
				if (errors.hasOwnProperty(key)) {
					errors[key].forEach((value: any) => result.push(value));
				}
			}
		}
		return result;
	}

	private getTranslationValues(values) {
		const result = [];
		if (values) {
			for (const key in values) {
				if (values.hasOwnProperty(key)) {
					result.push(values[key]);
				}
			}
		}
		return result;
	}
}
