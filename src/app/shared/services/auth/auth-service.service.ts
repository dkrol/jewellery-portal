import { Injectable } from '@angular/core';
import { LocalStorageService } from '../storage/local-storage.service';
import { JwtToken } from '../../interfaces/JwtToken';
import { AuthToken } from '../../interfaces/AuthToken';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { PortalService } from '../portal/portal.service';

export function tokenGetter() {
	const jwtToken: JwtToken = JSON.parse(localStorage.getItem('jwtToken')) as JwtToken;
	return jwtToken ? jwtToken.token : '';
}

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	private readonly TOKEN_KEY = 'jwToken';
	private readonly jwtHelperService = new JwtHelperService();

	public get isLogged(): boolean { return this.isUserLogged(); }
	public get isAdminRole(): boolean { return this.isAdminRoleLogged(); }
	public get isCustomerRole(): boolean { return this.isCustomerRoleLogged(); }
	public get login(): string { return this.getAuthLogin(); }

	constructor(
		private localStorageService: LocalStorageService,
		private http: HttpClient,
		private router: Router,
		private portalService: PortalService) { }

	public setAuthToken(jwtToken: JwtToken) {
		this.localStorageService.setItem(this.TOKEN_KEY, jwtToken);
		this.initAuthTokenRefresh(jwtToken);
	}

	public getAuthToken(): JwtToken {
		return this.localStorageService.getItem(this.TOKEN_KEY) as JwtToken;
	}

	public getAuthTokenValue(): string {
		const jwtToken: JwtToken = this.getAuthToken();
		return jwtToken ? jwtToken.token : '';
	}

	public getAuthTokenDecoded(): AuthToken {
		const authToken = this.getAuthToken();
		if (authToken) {
			return this.jwtHelperService.decodeToken(authToken.token) as AuthToken;
		}
		return null;
	}

	public getAuthId(): number {
		const authTokenDecoded = this.getAuthTokenDecoded();
		return authTokenDecoded ? authTokenDecoded.userId : -1;
	}

	public getAuthLogin(): string {
		const authTokenDecoded = this.getAuthTokenDecoded();
		return authTokenDecoded ? authTokenDecoded.login : '';
	}

	public getAuthRole(): string {
		const authTokenDecoded = this.getAuthTokenDecoded();
		return authTokenDecoded ? authTokenDecoded.role : '';
	}

	public logout() {
		this.portalService.hideMenu();
		this.removeAuthToken();
		this.router.navigate(['/login']);
	}

	public removeAuthToken() {
		this.localStorageService.removeItem(this.TOKEN_KEY);
	}

	public authTokenRefresh() {
		if (this.getAuthToken()) {
			this.authTokenRefreshAsync()
				.subscribe(
					response => this.setAuthToken(response),
					error => console.error(error));
		}
	}

	private isUserLogged(): boolean {
		const jwtToken: JwtToken = this.getAuthToken();
		if (!jwtToken) {
			return false;
		}

		const jwtDecoded = this.jwtHelperService.decodeToken(jwtToken.token);
		if (this.isExpired(jwtDecoded.exp)) {
			this.logout();
			return false;
		}

		return true;
	}

	private isAdminRoleLogged(): boolean {
		const role = this.getAuthRole();
		return role.toLocaleLowerCase() === 'admin';
	}

	private isCustomerRoleLogged(): boolean {
		const role = this.getAuthRole();
		return role.toLocaleLowerCase() === 'customer';
	}

	private initAuthTokenRefresh(jwtToken: JwtToken) {
		const jwtDecoded: AuthToken = this.jwtHelperService.decodeToken(jwtToken.token);
		const refreshTimeMs = (jwtDecoded.exp - jwtDecoded.iat - 60) * 1000;
		setTimeout(() => this.authTokenRefresh(), refreshTimeMs);
	}

	private authTokenRefreshAsync() {
		return this.http.post<JwtToken>('/api/auth/refresh', {});
	}

	private isExpired(timestamp: number): boolean {
		return timestamp < Math.round((new Date()).getTime() / 1000);
	}
}
