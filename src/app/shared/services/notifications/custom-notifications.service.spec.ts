import { TestBed } from '@angular/core/testing';

import { CustomNotificationsService } from './custom-notifications.service';

describe('CustomNotificationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomNotificationsService = TestBed.get(CustomNotificationsService);
    expect(service).toBeTruthy();
  });
});
