import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService } from 'angular2-notifications';

@Injectable({
	providedIn: 'root'
})
export class CustomNotificationsService {
	constructor(private notificationsService: NotificationsService, private translate: TranslateService) { }

	public success(title: any, content?: any) {
		this.notificationsService.success(title, content);
	}
	public info(title: any, content?: any) {
		this.notificationsService.info(title, content);
	}
	public warn(title: any, content?: any) {
		this.notificationsService.warn(title, content);
	}
	public error(title: any, content?: any) {
		this.notificationsService.error(title, content);
	}

	public successTranslate(titleCode: string, contentCode?: string) {
		this.translate.get([titleCode, contentCode])
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.success(translationValues[0], translationValues[1]);
			});
	}
	public infoTranslate(titleCode: string, contentCode?: string) {
		this.translate.get([titleCode, contentCode])
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.info(translationValues[0], translationValues[1]);
			});
	}
	public warnTranslate(titleCode: string, contentCode?: string) {
		this.translate.get([titleCode, contentCode])
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.warn(translationValues[0], translationValues[1]);
			});
	}
	public errorTranslate(titleCode: string, contentCode?: string) {
		this.translate.get([titleCode, contentCode])
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.error(translationValues[0], translationValues[1]);
			});
	}

	public successTranslateMany(titleCode: string, contentCodes: Array<string>) {
		contentCodes.unshift(titleCode);
		this.translate.get(contentCodes)
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.success(translationValues[0], translationValues.slice(1).join('<br>'));
			});
	}
	public infoTranslateMany(titleCode: string, contentCodes: Array<string>) {
		contentCodes.unshift(titleCode);
		this.translate.get(contentCodes)
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.info(translationValues[0], translationValues.slice(1).join('<br>'));
			});
	}
	public warnTranslateMany(titleCode: string, contentCodes: Array<string>) {
		contentCodes.unshift(titleCode);
		this.translate.get(contentCodes)
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.warn(translationValues[0], translationValues.slice(1).join('<br>'));
			});
	}
	public errorTranslateMany(titleCode: string, contentCodes: Array<string>) {
		contentCodes.unshift(titleCode);
		this.translate.get(contentCodes)
			.subscribe(response => {
				const translationValues = this.getTranslationValues(response);
				this.error(translationValues[0], translationValues.slice(1).join('<br>'));
			});
	}

	private getTranslationValues(values) {
		const result = [];
		if (values) {
			for (const key in values) {
				if (values.hasOwnProperty(key)) {
					result.push(values[key]);
				}
			}
		}
		return result;
	}
}
