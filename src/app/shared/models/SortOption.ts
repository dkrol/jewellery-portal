import { SortInfo } from './SortInfo';

export class SortOption {
	public code: string;
	public sortInfo: SortInfo;

	private constructor(code: string, sortInfo: SortInfo) {
		this.code = code;
		this.sortInfo = sortInfo;
	}

	public static Create(code: string, sortInfo: SortInfo): SortOption {
		return new SortOption(code, sortInfo);
	}
}
