export class PageInfo {
	offset: number;
	limit: number;

	public static Create(offset: number, limit: number) {
		return new PageInfo(offset, limit);
	}

	private constructor(offset: number, limit: number) {
		this.offset = offset;
		this.limit = limit;
	}
}
