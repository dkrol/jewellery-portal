export class UploadImage {
	public name: string;
	public type: string;
	public file: File;
	public url: any;

	public static CreateByFile(file: File): UploadImage {
		const uploadImage = new UploadImage();
		uploadImage.file = file;
		uploadImage.name = file.name;
		uploadImage.type = file.type;
		return uploadImage;
	}

	public static CreateByImage(imageName: any): UploadImage {
		const uploadImage = new UploadImage();
		uploadImage.name = imageName;
		uploadImage.url = `${window.location.origin}/images/${imageName}`;
		return uploadImage;
	}
}
