export class SortInfo {
	property: string;
	direction: string;

	public static Create(property: string, direction: string) {
		return new SortInfo(property, direction);
	}

	private constructor(property: string, direction: string) {
		this.property = property;
		this.direction = direction;
	}
}
