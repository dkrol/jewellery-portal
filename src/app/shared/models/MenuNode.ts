export class MenuNode {
	nameKey: string;
	router: string;
	iconName: string;
	isEnabled: boolean;

	constructor(nameKey: string, router: string, iconName: string) {
		this.nameKey = nameKey;
		this.router = router;
		this.iconName = iconName;
		this.isEnabled = false;
	}
}
