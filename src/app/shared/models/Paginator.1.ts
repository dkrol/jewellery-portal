export class Paginator {
	public pageIndex: number;
	public pageSize: number;
	public length: number;
	public pageSizeOptions: Array<number> = [1, 5, 10, 25, 100];

	constructor() {
		this.pageSize = 25;
	}
}