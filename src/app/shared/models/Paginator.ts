import { MatPaginator } from '@angular/material';

export class Paginator {
	public pageIndex: number;
	public pageSize: number;
	public length: number;
	public pageSizeOptions: Array<number> = [1, 5, 10, 25, 100];

	private constructor() {
		this.pageSize = 25;
	}

	public static create(): Paginator {
		return new Paginator();
	}

	public withPageSize(pageSize: number): Paginator {
		this.pageSize = pageSize;
		return this;
	}

	public updatePaginator(paginator: MatPaginator) {
		paginator.pageSize = this.pageSize;
		paginator.pageIndex = this.pageIndex;
		paginator.pageSize = this.pageSize;
		paginator.length = this.length;
		paginator.pageSizeOptions = this.pageSizeOptions;
	}
}