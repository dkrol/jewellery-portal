export class Paged<T> {
	public results: Array<T>;
	public offset: number;
  public limit: number;
  public totalCount: number;
  public pageCount: number;
  public sortProperty: string;
  public sortDirection: string;
}