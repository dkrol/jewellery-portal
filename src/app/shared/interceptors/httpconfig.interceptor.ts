import { Injectable } from '@angular/core';
import {
	HttpInterceptor,
	HttpRequest,
	HttpResponse,
	HttpHandler,
	HttpEvent,
	HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpErrorService } from '../services/http/http-error.service';
import { AuthService } from '../services/auth/auth-service.service';
import { PortalService } from '../services/portal/portal.service';

@Injectable() export class HttpConfigInterceptor implements HttpInterceptor {

	constructor(private authService: AuthService, private httpErrorService: HttpErrorService, private portalService: PortalService) { }

	public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		request = this.updateRequest(request);

		return next.handle(request).pipe(
			map((event: HttpEvent<any>) => this.handleRequest(event)),
			catchError((error: HttpErrorResponse) => this.handleResponseError(error))
		);
	}

	private updateRequest(request: HttpRequest<any>): HttpRequest<any> {
		const token: string = this.authService.getAuthTokenValue();
		if (token) {
			request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
		}

		const ignore = typeof request.body === 'undefined'
			|| request.body === null
			|| request.body.toString() === '[object FormData]'
			|| request.headers.has('Content-Type');

		if (!ignore) {
			request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
		}

		request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
		return request;
	}

	private handleRequest(event: HttpEvent<any>) {
		if (event instanceof HttpResponse) {
			// console.log('Event: ', event);
		}
		return event;
	}

	private handleResponseError(httpErrorResponse: HttpErrorResponse) {
		this.httpErrorService.notifyUser(httpErrorResponse);
		this.portalService.isLoading = false;
		return throwError(this.prepareResponseError(httpErrorResponse));
	}

	private prepareResponseError(httpErrorResponse: HttpErrorResponse) {
		return {
			reason: httpErrorResponse && httpErrorResponse.error && httpErrorResponse.error.reason
				? httpErrorResponse.error.reason : '',
			status: httpErrorResponse.status
		};
	}
}
