export interface AuthToken {
	userId: number;
	login: string;
	role: string;
	nbf: number;
	exp: number;
	iat: number;
}
