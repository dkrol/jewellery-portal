import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

export class UserEmailForm {
	private _form: FormGroup;
	private _formBuilder: FormBuilder;

	public get form(): FormGroup {
		return this._form;
	}

	constructor() {
		this._formBuilder = new FormBuilder();
		this.initFormGroup();
	}

	public loadData(user: any) {
		this._form.patchValue({
			id: user.id,
			email: user.email
		});
	}

	public parseJson(): any {
		const result = this._form.getRawValue();
		return result;
	}

	public clear() {
		this._form.reset();
		this.initFormGroup();
	}

	private initFormGroup() {
		this._form = this._formBuilder.group({
			id: new FormControl(''),
			email: new FormControl('', [Validators.required, Validators.email])
		}, { validator: [this.emailValidation('email')] });
	}

	private emailValidation(matchingControlName: string) {
		return (formGroup: FormGroup) => {
			const matchingControl = formGroup.controls[matchingControlName];
			if (matchingControl.errors && !matchingControl.errors.emailFormat) {
				return;
			}

			// tslint:disable-next-line:max-line-length
			const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			if (!re.test(String(matchingControl.value).toLowerCase())) {
				matchingControl.setErrors({ emailFormat: true });
			} else {
				matchingControl.setErrors(null);
			}
		};
	}
}
