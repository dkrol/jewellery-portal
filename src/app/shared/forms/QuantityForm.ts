import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

export class QuantityForm {
	private _form: FormGroup;
	private _formBuilder: FormBuilder;
	private _min: number;
	private _max: number;

	public get form(): FormGroup {
		return this._form;
	}

	public get min(): number {
		return this._min;
	}

	public get max(): number {
		return this._max;
	}

	constructor(min: number, max: number) {
		this._min = min;
		this._max = max;
		this._formBuilder = new FormBuilder();
		this.initFormGroup();
	}

	public loadData(quantity: number) {
		this._form.patchValue({
			quantity
		});
	}

	public parseJson(): any {
		const result = this._form.getRawValue();
		return result;
	}

	public clear() {
		this._form.reset();
		this.initFormGroup();
	}

	private initFormGroup() {
		this._form = this._formBuilder.group({
			quantity: new FormControl('', [Validators.required, Validators.min(this._min), Validators.max(this._max)]),
		});
	}
}
