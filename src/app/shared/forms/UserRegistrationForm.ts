import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AddUserCommand } from '../domains/users/commands/AddUserCommand';
import { UserEmailForm } from './UserEmailForm';
import { UserPasswordForm } from './UserPasswordForm';

export class UserRegisterForm {
	private _form: FormGroup;
	private _formBuilder: FormBuilder;
	private _userEmailForm = new UserEmailForm();
	private _userPasswordForm = new UserPasswordForm();

	public get form(): FormGroup { return this._form; }
	public get passwordMinLength() { return this._userPasswordForm.passwordMinLength; }

	constructor() {
		this._formBuilder = new FormBuilder();
		this.initFormGroup();
	}

	public parseJson(): AddUserCommand {
		const result = this._form.getRawValue();
		delete result.confirmPassword;
		return result as AddUserCommand;
	}

	public clear() {
		this._form.reset();
		this.initFormGroup();
	}

	private initFormGroup() {
		this._form = this._formBuilder.group({
			login: new FormControl('', [Validators.required, Validators.minLength(3)]),
			...this._userEmailForm.form.controls,
			...this._userPasswordForm.form.controls
		}, {
				validators: [this._userEmailForm.form.validator, this._userPasswordForm.form.validator]
			});
	}
}
