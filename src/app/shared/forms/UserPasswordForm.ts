import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

export class UserPasswordForm {
	public readonly passwordMinLength = 8;

	private _form: FormGroup;
	private _formBuilder: FormBuilder;

	public get form(): FormGroup {
		return this._form;
	}

	constructor() {
		this._formBuilder = new FormBuilder();
		this.initFormGroup();
	}

	public loadData(user: any) {
		this._form.patchValue({
			id: user.id
		});
	}

	public parseJson(): any {
		const result = this._form.getRawValue();
		delete result.confirmPassword;
		return result;
	}

	public clear() {
		this._form.reset();
		this.initFormGroup();
	}

	private initFormGroup() {
		this._form = this._formBuilder.group({
			id: new FormControl(''),
			password: new FormControl('', [Validators.required, Validators.minLength(this.passwordMinLength)]),
			confirmPassword: new FormControl('', [Validators.required, Validators.minLength(this.passwordMinLength)]),
		}, {
				validator: [this.passwordValidation('password', 'confirmPassword')]
			});
	}

	private passwordValidation(controlName: string, matchingControlName: string) {
		return (formGroup: FormGroup) => {
			const control = formGroup.controls[controlName];
			const matchingControl = formGroup.controls[matchingControlName];

			if (matchingControl.errors && !matchingControl.errors.mustMatch) {
				return;
			}

			if (control.value !== matchingControl.value) {
				matchingControl.setErrors({ mustMatch: true });
			} else {
				matchingControl.setErrors(null);
			}
		};
	}
}
