import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

export class UserLoginForm {

	private _form: FormGroup;
	private _formBuilder: FormBuilder;

	public get form() {
		return this._form;
	}

	constructor() {
		this._formBuilder = new FormBuilder();
		this.initFormGroup();
	}

	public parseJson(): any {
		const result = this._form.getRawValue();
		return result;
	}

	private initFormGroup() {
		this._form = this._formBuilder.group({
			login: new FormControl('', [Validators.required]),
			password: new FormControl('', [Validators.required])
		});
	}
}
