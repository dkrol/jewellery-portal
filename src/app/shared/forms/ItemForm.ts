import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { UploadImage } from '../models/UploadImage';

export class ItemForm {
	public readonly titleMaxLength: number = 60;
	private readonly _imageValidExtensions = ['.jpg', '.jpeg', '.bmp', '.gif', '.png'];

	private _form: FormGroup;
	private _formBuilder: FormBuilder;

	public get form() { return this._form; }
	public get images(): Array<UploadImage> {
		return this.imagesFormArray
			? this.imagesFormArray.controls.map(control => control.value)
			: [];
	}

	private get imagesFormArray(): FormArray {
		return this._form.get('images') as FormArray;
	}

	constructor() {
		this._formBuilder = new FormBuilder();
		this.initFormGroup();
		this.setDefaultValue();
	}

	public loadData(item: any) {
		this._form.patchValue({
			title: item.title,
			categoryId: item.categoryId,
			description: item.description,
			material: item.material,
			weight: item.weight,
			price: item.price,
			quantity: item.quantity,
			isActive: item.isActive
		});

		const uploadImages = item.images.map(image => UploadImage.CreateByImage(image));
		uploadImages.forEach(image => this.imagesFormArray.push(new FormControl(image)));
	}

	public getImagesFormData(): FormData {
		const formData = new FormData();
		const files = this.getImageFiles();
		files.forEach(file => formData.append('files', file));
		return formData;
	}

	public addImages(files: Array<File>) {
		files.forEach((file: File) => this.addImage(file));
	}

	public addImage(file: File) {
		const uploadImage = UploadImage.CreateByFile(file);
		const fileReader = new FileReader();

		fileReader.onload = (event) => {
			uploadImage.url = fileReader.result + '';
			this.imagesFormArray.push(new FormControl(uploadImage));
		};

		fileReader.readAsDataURL(file);
	}

	public deleteImage(image: UploadImage) {
		const control = this.imagesFormArray.controls.find(c => c.value === image);
		const indexControl = this.imagesFormArray.controls.indexOf(control);
		this.imagesFormArray.controls.splice(indexControl, 1);

		if (this.imagesFormArray.controls.length === 0) {
			this.imagesFormArray.reset();
		}
	}

	public parseJsonToAdd(images: Array<string>) {
		const result = this._form.getRawValue();
		result.images = images.map((name: string) => ({ name }));
		return result;
	}

	public parseJsonToUpdate(images: Array<string>) {
		const result = this._form.getRawValue();
		result.images = this.prepareExistingImages(result.images);
		result.images = this.prepareUpdateImages(result.images, images);
		return result;
	}

	private getImageFiles(): Array<File> {
		return this.images
			.filter(image => image.file)
			.map(image => image.file);
	}

	private prepareExistingImages(images) {
		return images
			.filter((image: UploadImage) => !image.file)
			.map((image: UploadImage) => ({ name: image.name }));
	}

	private prepareUpdateImages(existingImages: any, uploadImages: Array<string>): any {
		const uploadImagesParse = uploadImages.map(name => ({ name }));
		return existingImages.concat(uploadImagesParse);
	}

	private initFormGroup() {
		this._form = this._formBuilder.group({
			title: new FormControl('', [Validators.required, Validators.maxLength(this.titleMaxLength)]),
			categoryId: new FormControl('', [Validators.required]),
			description: new FormControl('', [Validators.required]),
			material: new FormControl('', [Validators.required]),
			weight: new FormControl('', [Validators.required]),
			price: new FormControl('', [Validators.required]),
			quantity: new FormControl('', [Validators.required]),
			isActive: new FormControl(''),
			images: this._formBuilder.array([], [Validators.required])
		});
	}

	private setDefaultValue() {
		this._form.patchValue({
			isActive: true
		});
	}
}
