import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({
	selector: '[jwHeightByWidth]'
})
export class HeightByWidthDirective implements OnInit {
	constructor(private el: ElementRef) { }

	ngOnInit() {
		setTimeout(() => this.updateElHeigth());
	}

	@HostListener('window:resize', ['$event'])
	onResize() {
		this.updateElHeigth();
	}

	private updateElHeigth() {
		this.el.nativeElement.style.height = `${this.el.nativeElement.clientWidth}px`;
	}
}
