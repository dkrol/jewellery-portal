import { Directive, ElementRef, Input, OnInit, HostListener } from '@angular/core';

@Directive({
	selector: '[jwShowHover]'
})
export class ShowHoverDirective implements OnInit {
	@Input() jwShowHoverWidth: number;
	@Input() jwShowHoverHeight: number;
	@Input() jwShowHoverDelay: number;

	private defaultDelay = 1;

	constructor(private element: ElementRef) { }

	ngOnInit() {
		this.initElement();
	}

	@HostListener('mouseout') onMouseOut() {
		if (this.jwShowHoverWidth) {
			this.updateElementWidth(this.jwShowHoverWidth);
		}
		if (this.jwShowHoverHeight) {
			this.updateElementHeight(this.jwShowHoverHeight);
		}
	}

	@HostListener('mouseover') onMouseOver() {
		const children = Array.from(this.element.nativeElement.children);
		const width = children.map((child: any) => child.clientWidth).reduce((a, b) => a + b);
		const height = children.map((child: any) => child.clientHeight).reduce((a, b) => a + b);

		if (this.jwShowHoverWidth) {
			this.updateElementWidth(width);
		}

		if (this.jwShowHoverHeight) {
			this.updateElementHeight(height);
		}
	}

	private initElement() {
		this.updateElementWidth(this.jwShowHoverWidth);
		this.updateElementHeight(this.jwShowHoverHeight);
		this.element.nativeElement.style.overflow = 'hidden';
		this.element.nativeElement.style.transition = this.jwShowHoverDelay
			? `all ${this.jwShowHoverDelay}s` : `all ${this.defaultDelay}s`;
	}

	private updateElementWidth(width) {
		this.element.nativeElement.style.width = `${width}px`;
	}

	private updateElementHeight(height) {
		this.element.nativeElement.style.height = `${height}px`;
	}
}
