export class AddItemCommand {
	title: string;
	categoryId: number;
	description: string;
	material: string;
	weight: number;
	price: number;
	quantity: number;
	images: Array<string>;
}
