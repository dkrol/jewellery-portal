import { PageInfo } from 'src/app/shared/models/PageInfo';
import { SortInfo } from 'src/app/shared/models/SortInfo';

export class GetItemsFilterPageQuery {
	public pageInfo: PageInfo;
	public sortInfo: SortInfo;
	public searchText: string;
	public isActive: boolean;
	// Another params as sold, price range, weight itp.

	constructor() {
		this.pageInfo = PageInfo.Create(0, 0);
	}
}
