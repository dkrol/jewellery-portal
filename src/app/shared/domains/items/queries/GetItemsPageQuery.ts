import { PageInfo } from 'src/app/shared/models/PageInfo';
import { SortInfo } from 'src/app/shared/models/SortInfo';

export class GetItemsPageQuery {
	public pageInfo: PageInfo;
	public sortInfo: SortInfo;
	public searchText: string;

	constructor() {
		this.pageInfo = PageInfo.Create(0, 0);
	}
}
