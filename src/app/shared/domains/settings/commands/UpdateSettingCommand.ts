export class UpdateSettingCommand {
	public id: number;
	public isNewsletterNotification: boolean;
	public isNewMessageNotification: boolean;
}