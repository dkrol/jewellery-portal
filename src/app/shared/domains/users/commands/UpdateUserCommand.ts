export class UpdateUserCommand {
	public userId: number;
	public email: string;
	public roleId: number;
	public isActive: boolean;
}
