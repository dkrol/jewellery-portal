export class ChangeUserEmailCommand {
	public id: number;
	public email: string;
}
