import { Component, ViewEncapsulation } from '@angular/core';
import { PortalService } from './shared/services/portal/portal.service';
import { LocalStorageService } from './shared/services/storage/local-storage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'jw-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less'],
	encapsulation: ViewEncapsulation.None
})
export class AppComponent {
	private readonly LANGUAGE_KEY = 'jwLanguage';
	private readonly DEFAULT_LANGUAGE: string = 'pl';

	public isLoading: boolean = false;

	constructor(
		public portalService: PortalService,
		private translate: TranslateService,
		private localStorageService: LocalStorageService) {
		this.initLanguage();
	}

	private initLanguage() {
		this.translate.setDefaultLang(this.DEFAULT_LANGUAGE);
		const selectedLanguage = this.localStorageService.getItem(this.LANGUAGE_KEY);
		if (!selectedLanguage) {
			this.translate.use(this.DEFAULT_LANGUAGE);
			this.localStorageService.setItem(this.LANGUAGE_KEY, this.DEFAULT_LANGUAGE);
		} else {
			this.translate.use(selectedLanguage);
		}
	}
}
